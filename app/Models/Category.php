<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Category extends Model
{
    use HasFactory;

    protected $table = "categories";

    protected $fillable = [
        "name",
        "parent_id",
        "main_default"
    ];

    public function GetTable() 
    {
        return $this->table;
    }

    /* New!! */

    public function GetCategoryByParentId($parent_id) 
    {
        return DB::table($this->table)->where("parent_id", $parent_id)->get();
    }

    public function GetByProductsCategoryId($category_id) 
    {
        return DB::table($this->table)
            ->where($this->table . ".id", $category_id)
            ->leftJoin("category_types", "category_types.category_id", "=", $this->table . ".id")
            ->leftJoin("products", "products.category_type_id", "=", "category_types.id")
            ->rightJoin("product_images", "product_images.product_id", "=", "products.id")
            ->get();
    }
}
