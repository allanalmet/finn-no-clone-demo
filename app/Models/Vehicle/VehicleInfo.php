<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleInfo extends Model
{
    use HasFactory;

    protected $table = "product_car_info";

    protected $fillable = [
        "name",
    ];

    public function GetTable() 
    {
        return $this->table;
    }

}
