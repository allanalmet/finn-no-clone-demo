<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleTransmission extends Model
{
    use HasFactory;

    protected $table = "product_car_transmissions";

    protected $fillable = [
        "name",
    ];

    public function GetTable() 
    {
        return $this->table;
    }

}
