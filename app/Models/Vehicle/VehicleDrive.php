<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleDrive extends Model
{
    use HasFactory;

    protected $table = "product_car_drives";

    protected $fillable = [
        "name",
    ];

    public function GetTable() 
    {
        return $this->table;
    }

}
