<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductImage extends Model
{
    use HasFactory;

    protected $table = "product_images";

    protected $fillable = [
        "image",
        "default",
        "product_id"
    ];

    public function GetTable() 
    {
        return $this->table;
    }


    public function GetImagesByProductId($id) 
    {
        return DB::table($this->table)->where("product_id", $id)->get();
    }
}
