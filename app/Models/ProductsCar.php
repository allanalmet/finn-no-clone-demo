<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsCar extends Model
{
    use HasFactory;

    protected $table = "products_car";

    protected $fillable = [
        "product_id",
        "body_style_id",
        "model_year",
        "price",
        "power",
        "fuel_type_id",
        "transmission_id",
        "drive_id",
        "private",
        "sale_types_id",
        "year_model",
        "milage",
        "location",
        "seats",
        "product_car_colors_id",
        "product_car_drives_id",
        "product_car_transmissions_id",
    ];

    public function GetTable() 
    {
        return $this->table;
    }
}
