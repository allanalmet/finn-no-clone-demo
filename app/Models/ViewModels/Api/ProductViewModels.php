<?php

namespace App\Models\ViewModels\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductViewModels
{
    protected $id;

    protected $name;

    protected $description;

    protected $category_id;

    protected $price;

    public function __construct($id, $name, $description, $category_id, int $price)
    {
        $this->id = $id;        
        $this->name = $name;
        $this->description = $description;
        $this->category_id = $category_id;
        $this->price = $price;
    }

    public function GetId(){
        return $this->id;
    }

    public function GetName(){
        return $this->name;
    }

    public function GetDescription(){
        return $this->description;
    }

    public function GetCategoryId(){
        return $this->category_id;
    }

    public function GetPriceDouble(){
        return $this->price / 100;
    }
    
    public function ToJson(){
        return json_encode($this);
    }
}
