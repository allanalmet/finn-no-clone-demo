<?php

namespace App\Models\ViewModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryViewModels
{
    protected $id;

    protected $name;

    protected $parent_id;

    public function __construct($id, $name, $parent_id)
    {
        $this->id = $id;        
        $this->name = $name;
        $this->parent_id = $parent_id;
    }

    public function GetName(){
        return $this->name;
    }

    public function GetId(){
        return $this->id;
    }

    public function GetParentId(){
        return $this->parent_id;
    }

}
