<?php

namespace App\Models\ViewModels;

class UserViewModels {

    private $name;

    private $email;

    public function __construct($name, $email){
        $this->name = $name;
        $this->email = $email;
    }

    public function GetName(){
        return $this->name;
    }

    public function GetEmail(){
        return $this->email;
    }
}

