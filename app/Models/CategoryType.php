<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoryType extends Model
{
    use HasFactory;

    protected $table = "category_types";

    protected $fillable = [
        "type_label",
        "category_id"
    ];

    public function GetTable() 
    {
        return $this->table;
    }


    public function GetTypesByCategoryId($category_id) 
    {
        return DB::table($this->table)->where("category_types.category_id", $category_id)->get();
    }




}
