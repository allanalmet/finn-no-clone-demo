<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = [
        "name",
        "description",
        "category_type_id",
        "user_id",
        "price",
        "is_ad"
    ];

    public function GetTable() 
    {
        return $this->table;
    }


    public function GetAllByUserId($user_id) 
    {
        return DB::table($this->table)->where("user_id", $user_id)->get();
    }

    public function GetSingleById($id) 
    {
        return DB::table($this->table)->where("id", $id)->first();
    }

    public function GetByCategoryId($category_id) 
    {
        return DB::table($this->table)->where("category_type_id", $category_id)->get();
    }
}