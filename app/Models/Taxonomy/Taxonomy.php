<?php

namespace App\Models\Taxonomy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model
{
    use HasFactory;

    protected $table = "taxonomy";

    protected $fillable = [
        "name",
        "title",
    ];

    public function GetTable() 
    {
        return $this->table;
    }


}
