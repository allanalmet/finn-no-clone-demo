<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductVehicleInfo extends Model
{
    use HasFactory;

    protected $table = "products_car";

    protected $fillable = [
        "product_id",
        "body_style_id",
        "model_year",
        "price",
        "power",
        "fuel_type_id",
        "transmission_id",
        "drive_id",
    ];

    public function GetTable() 
    {
        return $this->table;
    }
}