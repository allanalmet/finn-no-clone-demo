<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTaxonomy extends Model
{
    use HasFactory;

    protected $table = "product_taxonomies";

    protected $fillable = [
        "product_id",
        "taxonomy_id",
    ];

    public function GetTable() 
    {
        return $this->table;
    }
}
