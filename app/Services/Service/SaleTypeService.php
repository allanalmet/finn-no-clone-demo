<?php

namespace App\Services\Service;

use App\Services\Interfaces\SaleTypeServiceInterface;

use App\Repositories\SaleTypeRepository;
use Illuminate\Support\Facades\Validator;

class SaleTypeService implements SaleTypeServiceInterface {

    public function __construct(SaleTypeRepository $saleTypeRepository) 
    {
        $this->saleTypeRepository = $saleTypeRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->saleTypeRepository->Save($data);
    }

    public function Update($data)
    {
        return $this->saleTypeRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->saleTypeRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->saleTypeRepository->GetAll();

    }

}
