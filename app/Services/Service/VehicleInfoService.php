<?php

namespace App\Services\Service;

use App\Models\Settings;
use App\Services\Interfaces\VehicleInfoServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\VehicleInfoRepository;
use Illuminate\Support\Facades\Validator;

class VehicleInfoService implements VehicleInfoServiceInterface {

    public function __construct(VehicleInfoRepository $vehicleInfoRespository) 
    {
        $this->vehicleInfoRespository = $vehicleInfoRespository;
    }

    public function GetVehicleInfo($product_id) 
    {
        return $this->vehicleInfoRespository->GetAll();

    }

}
