<?php

namespace App\Services\Service;

use App\Services\Interfaces\VehicleTransmissionServiceInterface;

use App\Repositories\VehicleTransmissionRepository;
use Illuminate\Support\Facades\Validator;

class VehicleTransmissionService implements VehicleTransmissionServiceInterface {

    public function __construct(VehicleTransmissionRepository $vehicleTransmissionRepository) 
    {
        $this->vehicleTransmissionRepository = $vehicleTransmissionRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->vehicleTransmissionRepository->Save($data);
    }

    public function Update($data)
    {
        return $this->vehicleTransmissionRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->vehicleTransmissionRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->vehicleTransmissionRepository->GetAll();

    }

}
