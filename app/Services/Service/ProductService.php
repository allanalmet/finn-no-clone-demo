<?php

namespace App\Services\Service;

use App\Models\Category;
use App\Models\Product;
use App\Models\CategoryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Interfaces\ProductServiceInterface;

use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Validator;

// TODO VAATA ET KÕIK SAAKS ÕIGESTI; ALL ON CATEGORY_ID'd
class ProductService implements ProductServiceInterface
{

    private $productRepository;

    public function __construct(ProductRepository $productRepository) 
    {
        $this->productRepository = $productRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required',
            'location' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }


        return $this->productRepository->Store($data);
    }

    public function Update($data) 
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->productRepository->Update($data);
    }

    
    public function GetAll()
    {
        return $this->productRepository->GetAll();
    }

    public function GetAllByUserId($user_id)
    {
        $products = (new Product())->GetAllByUserId($user_id);

        return $products;
        $products = Product::where("user_id", "=", $user_id)->get();

        return $products;
    }

    public function GetSingleById($id)
    {
        return $this->productRepository->GetById($id);
    }

    public function GetByParentCategoryId($category_id) 
    {
        return (new Category())->GetByProductsCategoryId($category_id);
        //return Product::where("category_id", $category_id)->get();
    }

    /*
    public function GetByCategoryId($category_id) 
    {
        return (new CategoryType())->GetByCategoryId($category_id);
        //return Product::where("category_id", $category_id)->get();
    }
    */

    public function GetByCategoryTypeId($category_id) 
    {
        return $this->productRepository->GetByCategoryTypeId($category_id);
    }

}