<?php

namespace App\Services\Service;

use App\Models\Category;
use App\Models\Product;
use App\Models\CategoryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Services\Interfaces\ProductImagesServiceInterface;

use App\Repositories\ProductImagesRepository;
use Illuminate\Support\Facades\Validator;

class ProductImagesService implements ProductImagesServiceInterface
{

    private $productImagesRepository;

    public function __construct(ProductImagesRepository $productImagesRepository) 
    {
        $this->productImagesRepository = $productImagesRepository;
    }

    public function Store($data) 
    {

    }

    public function StoreAll($data)
    {
        $images = [];
        $validate = Validator::make($data, [
            'product_id' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }
        
        $imagesData = json_decode($data["images"]);
        $count = count($imagesData);
        $product_id = $data["product_id"];
        if(count($imagesData) > 0):
            foreach($imagesData as $image):
                try {
                    if(strlen($image->dataURL) > 128) {
                        list($mime, $data)   = explode(';', $image->dataURL);
                        list(, $data)       = explode(',', $data);


                        $mime = explode(':',$mime)[1];
                        $ext = explode('/',$mime)[1];
                        $name = mt_rand().time();
                        $savePath = 'img/'.$name.'.'.$ext;

                        $images[] = [
                            "image" => $name.'.'.$ext,
                            "default" => 0,
                            "product_id" => $product_id
                        ];

                        $data = base64_decode($data);
                        file_put_contents(public_path().'/'.$savePath, $data);
                    }
                }
                catch (\Exception $e) {
                    //doing nothing here for not breaking the loop
                  // you can pass the error message to your view if you want.
                }
            endforeach;
        endif;

        if (!$this->productImagesRepository->StoreAll($images)) return false;

        return true;
    }

    public function Update($data) 
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->productImagesRepository->Update($data);
    }

    
    public function GetAll()
    {
        return $this->productImagesRepository->GetAll();
    }

    public function GetAllByUserId($user_id)
    {
        $products = (new Product())->GetAllByUserId($user_id);

        return $products;
        $products = Product::where("user_id", "=", $user_id)->get();

        return $products;
    }

    public function GetSingleById($id)
    {
        return $this->productImagesRepository->GetById($id);
    }

    public function GetByParentCategoryId($category_id) 
    {
        return (new Category())->GetByProductsCategoryId($category_id);
        //return Product::where("category_id", $category_id)->get();
    }

    public function GetByCategoryId($category_id) 
    {
        return (new CategoryType())->GetByCategoryId($category_id);
        //return Product::where("category_id", $category_id)->get();
    }

}