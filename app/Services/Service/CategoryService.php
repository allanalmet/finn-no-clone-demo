<?php

namespace App\Services\Service;

use App\Models\Category;
use App\Models\CategoryType; //TODO temporary fix
use App\Services\Interfaces\CategoryServiceInterface;

use Illuminate\Support\Facades\Validator;

use App\Repositories\CategoryRepository;
use App\Repositories\CategoryTypeRepository;

class CategoryService implements CategoryServiceInterface
{

    public function __construct(
        CategoryRepository $categoryRepository, 
        CategoryTypeRepository $categoryTypeRepository
    ) 
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryTypeRepository = $categoryTypeRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        $category = $this->categoryRepository->save($data);
        return $category;
    }

    public function remove($id){
        return $this->categoryRepository->remove($id);
    } 

    public function update($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        $category = $this->categoryRepository->save($data);
        return $category;
    }

    public function GetAll() 
    {
        return $this->categoryRepository->GetAll();
    }

    public function GetCategoryById($id) 
    {
        return $this->categoryRepository->GetById($id);
    }

    public function GetSubTypesByParentId($category) 
    {
        return $this->categoryTypeRepository->GetTypesByCategoryId($category);
    }

    public function GetSubCategoriesByParentId($category) 
    {
        return $this->categoryRepository->GetSubCategoriesById($category);
        
    }

    public function GetSubCategoryItemsByParentId($category, $id) 
    {
        return $this->categoryRepository->GetByCategoryId($id);
    }

    public function GetMainPageCategories() 
    {
        return $this->categoryRepository->GetMainPageCategories();
    }

    // NEW

    public function GetCategoryTypes($category_id) 
    {
        return (new CategoryType())->GetTypesByCategoryId($category_id);
    }

    public function GetByCategoryId() 
    {
        
    }
}
