<?php

namespace App\Services\Service;

use App\Models\Settings;
use App\Services\Interfaces\ProductTaxonomyServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\ProductTaxonomyRepository;
use Illuminate\Support\Facades\Validator;

class ProductTaxonomyService implements ProductTaxonomyServiceInterface {

    public function __construct(ProductTaxonomyRepository $productTaxonomyRespository) 
    {
        $this->productTaxonomyRespository = $productTaxonomyRespository;
    }


    public function Store($data)
    {
        $validate = Validator::make($data, [
            'taxonomies' => 'required|max:255',
            'product_id' => 'required',
            'name' => 'required|max:255',
            'title' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->productTaxonomyRespository->Store($data);
    }
    public function StoreAll($data)
    {
        $validate = Validator::make($data, [
            'taxonomies' => 'required|max:255',
            'product_id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }


        $taxonomies = json_decode($data["taxonomies"]);
        $taxonomiesData = [];
        
        foreach($taxonomies as $taxonomy) 
        {
            $taxonomiesData[] = [
                "product_id" => $data['product_id'],
                "taxonomy_id" => $taxonomy,

            ];
        }

        return $this->productTaxonomyRespository->StoreAll($taxonomiesData);
    }

    public function Update($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'title' => 'required',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->productTaxonomyRespository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->productTaxonomyRespository->GetById($id);
    }

    public function GetByProductId($id)
    {
        return $this->productTaxonomyRespository->GetByProductId($id);
    }

    public function GetAll() 
    {
        return $this->productTaxonomyRespository->GetAll();

    }

}
