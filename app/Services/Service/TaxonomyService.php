<?php

namespace App\Services\Service;

use App\Models\Settings;
use App\Services\Interfaces\TaxonomyServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\TaxonomyRepository;
use Illuminate\Support\Facades\Validator;

class TaxonomyService implements TaxonomyServiceInterface {

    public function __construct(TaxonomyRepository $taxonomyRespository) 
    {
        $this->taxonomyRespository = $taxonomyRespository;
    }


    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'title' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->taxonomyRespository->Store($data);
    }
    public function StoreAll($data)
    {
        return $this->taxonomyRespository->StoreAll($data);
    }

    public function Update($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'title' => 'required',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->taxonomyRespository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->taxonomyRespository->GetById($id);
    }

    public function GetByParentId($id)
    {
        return $this->taxonomyRespository->GetByParentId($id);
    }

    public function GetAll() 
    {
        return $this->taxonomyRespository->GetAll();

    }

}
