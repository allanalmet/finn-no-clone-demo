<?php

namespace App\Services\Service;

use App\Services\Interfaces\SearchServiceInterface;

use App\Repositories\ProductRepository;
use App\Repositories\CategoryTypeRepository;

use Illuminate\Support\Facades\Validator;

class SearchService implements SearchServiceInterface {

    public function __construct(
        ProductRepository $productRepository, CategoryTypeRepository $categoryTypeRepository) 
    {
        $this->productRepository = $productRepository;
        $this->categoryTypeRepository = $categoryTypeRepository;
    }

    public function GetAllByName($data) 
    {
        $products = $this->productRepository->GetAllByName($data["name"]);

        return $products;

    }

}
