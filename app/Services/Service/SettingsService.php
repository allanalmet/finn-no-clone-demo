<?php

namespace App\Services\Service;

use App\Models\Settings;
use App\Services\Interfaces\SettingsServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\SettingsRepository;
use Illuminate\Support\Facades\Validator;

class SettingsService implements SettingsServiceInterface {

    public function __construct(SettingsRepository $settingsRepository) 
    {
        $this->settingsRepository = $settingsRepository;
    }


    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->settingsRepository->Store($data);
    }

    public function Update($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->settingsRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->settingsRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->settingsRepository->GetAll();

    }

}
