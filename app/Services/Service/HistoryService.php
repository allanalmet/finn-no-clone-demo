<?php

namespace App\Services\Service;

use App\Models\History;
use App\Services\Interfaces\HistoryServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\HistoryRepository;
use Illuminate\Support\Facades\Validator;

class HistoryService implements HistoryServiceInterface {

    public function __construct(HistoryRepository $historyRepository) 
    {
        $this->historyRepository = $historyRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->historyRepository->Store($data);
    }

    public function Update($data)
    {
        return $this->historyRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->historyRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->historyRepository->GetAll();

    }

}
