<?php

namespace App\Services\Service;

use App\Services\Interfaces\VehicleColorServiceInterface;

use App\Repositories\VehicleColorRepository;
use Illuminate\Support\Facades\Validator;

class VehicleColorService implements VehicleColorServiceInterface {

    public function __construct(VehicleColorRepository $vehicleColorRepository) 
    {
        $this->vehicleColorRepository = $vehicleColorRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->vehicleColorRepository->Save($data);
    }

    public function Update($data)
    {
        return $this->vehicleColorRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->vehicleColorRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->vehicleColorRepository->GetAll();

    }

}
