<?php

namespace App\Services\Service;

use App\Models\Category;
use App\Models\Product;
use App\Models\CategoryType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Services\Interfaces\ProductVehicleInfoServiceInterface;

use App\Repositories\ProductVehicleInfoRepository;
use Illuminate\Support\Facades\Validator;

class ProductVehicleInfoService implements ProductVehicleInfoServiceInterface
{

    private $productVehicleInfoRepository;

    public function __construct(ProductVehicleInfoRepository $productVehicleInfoRepository) 
    {
        $this->productVehicleInfoRepository = $productVehicleInfoRepository;
    }

    public function Store($data) 
    {
        $validate = Validator::make($data, [
            'product_id'        => 'required|max:255',
            'car_color'         => 'required|max:255',
            'car_drive'         => 'required|max:255',
            'car_transmission'  => 'required|max:255',
            'sale_type'         => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }
        
        $this->productVehicleInfoRepository->Store($data);
    }

    public function StoreAll($data)
    {
        $validate = Validator::make($data, [
            'product_id'        => 'required|max:255',
            'car_color'         => 'required|max:255',
            'car_drive'         => 'required|max:255',
            'car_transmission'  => 'required|max:255',
            'sale_type'         => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }
        
        $this->productVehicleInfoRepository->StoreAll($data);
    }

    public function Update($data) 
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->productVehicleInfoRepository->Update($data);
    }

    
    public function GetAll()
    {
        return $this->productVehicleInfoRepository->GetAll();
    }


    public function GetSingleById($id)
    {
        return $this->productVehicleInfoRepository->GetById($id);
    }

}