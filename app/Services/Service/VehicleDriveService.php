<?php

namespace App\Services\Service;

use App\Services\Interfaces\VehicleDriveServiceInterface;

use App\Repositories\VehicleDriveRepository;
use Illuminate\Support\Facades\Validator;

class VehicleDriveService implements VehicleDriveServiceInterface {

    public function __construct(VehicleDriveRepository $vehicleDriveRepository) 
    {
        $this->vehicleDriveRepository = $vehicleDriveRepository;
    }

    public function Store($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        return $this->vehicleDriveRepository->Save($data);
    }

    public function Update($data)
    {
        return $this->vehicleDriveRepository->Update($data);
    }

    public function GetSingleById($id)
    {
        return $this->vehicleDriveRepository->GetById($id);
    }

    public function GetAll() 
    {
        return $this->vehicleDriveRepository->GetAll();

    }

}
