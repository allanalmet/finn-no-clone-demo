<?php

namespace App\Services\Interfaces;

interface VehicleColorServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}