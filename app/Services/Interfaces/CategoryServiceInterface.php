<?php

namespace App\Services\Interfaces;

use App\Models\Category;
use Illuminate\Http\Request;

interface CategoryServiceInterface
{

    public function Store($data);

    public function update($data);

    public function remove($id);

    public function GetAll();

    public function GetSubCategoriesByParentId($category);

    //public function GetSubCategoryItemsByParentId($category, $id);

    public function GetCategoryById($id);

    public function GetMainPageCategories();

    public function GetSubTypesByParentId($category); 

    public function GetCategoryTypes($category_id);

    
}
