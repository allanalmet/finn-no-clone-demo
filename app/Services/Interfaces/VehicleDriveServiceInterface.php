<?php

namespace App\Services\Interfaces;

interface VehicleDriveServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}