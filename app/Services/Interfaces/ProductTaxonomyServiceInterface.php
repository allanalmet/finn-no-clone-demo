<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface ProductTaxonomyServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);
    
    public function GetByProductId($id);

    public function GetAll();

    public function StoreAll($data);
}