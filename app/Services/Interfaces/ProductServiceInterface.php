<?php

namespace App\Services\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();

    public function GetAllByUserId($user_id);

    public function GetByParentCategoryId($category_id);

    //public function GetByCategoryId($category_id);

    public function GetByCategoryTypeId($category_id);
}