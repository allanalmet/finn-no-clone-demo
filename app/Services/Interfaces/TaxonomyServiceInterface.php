<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface TaxonomyServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);
    
    public function GetByParentId($id);

    public function GetAll();

    public function StoreAll($data);
}