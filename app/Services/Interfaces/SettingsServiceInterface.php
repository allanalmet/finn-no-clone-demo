<?php

namespace App\Services\Interfaces;

use App\Models\Settings;
use Illuminate\Http\Request;

interface SettingsServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}