<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface HistoryServiceInterface
{

    public function Store($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}