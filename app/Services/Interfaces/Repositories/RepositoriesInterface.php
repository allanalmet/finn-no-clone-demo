<?php

namespace App\Services\Interfaces\Repositories;

interface RepositoriesInterface
{

    public function Store($data);

    public function Update($data);

    public function Remove($id);

    public function GetAll();

    public function GetById($id);

    public function Save($data);

}
