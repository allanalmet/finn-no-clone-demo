<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface ProductVehicleInfoServiceInterface
{

    public function Store($data);

    public function StoreAll($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}