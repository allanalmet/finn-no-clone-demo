<?php

namespace App\Services\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductImagesServiceInterface
{

    public function Store($data);

    public function StoreAll($data);

    public function Update($data);

    public function GetSingleById($id);

    public function GetAll();
}