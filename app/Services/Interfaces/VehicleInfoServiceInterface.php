<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface VehicleInfoServiceInterface
{
    public function GetVehicleInfo($product_id);
}