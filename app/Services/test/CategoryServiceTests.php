<?php

namespace App\Services\Test;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\Interfaces\CategoryServiceInterface;

class CategoryServiceTests implements CategoryServiceInterface
{

    public function Store(Request $request) : Category
    {
        $category = new Category();
        $category->name = $request->category_name;
        $category->parent_id = 0 + 1;

        $category->save();

        return $category;
    }

    public function GetSingleByName($name) : Category
    {
        return new Category();
    }

    public function GetSubCategoriesByParentId($category) 
    {
        $faker = \Faker\Factory::create();

        $results = [];

        for ($i = 0; $i < 10; $i++) 
        {
            $category = new Category();
            $category->name = $faker->name;
            $category->parent_id = $category;
            $results[] = $category->toArray();
        }

        return $results;
    }

    public function GetSubCategoryItemsByParentId($category, $id) 
    {
        $faker = \Faker\Factory::create();

        $results = [];

        for ($i = 0; $i < 10; $i++) 
        {
            $category = new Category();
            $category->name = $faker->name;
            $category->parent_id = $id;
            $results[] = $category;
        }

        return $results;
    }
}
