<?php

namespace App\Services\Test;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\Interfaces\ProductServiceInterface;

class ProductServiceTests implements ProductServiceInterface
{
    public function Store(Request $request, int $user_id) : Product
    {
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->user_id = $user_id;

        $product->save();
        
        return $product;
    }

    
    public function GetAll()
    {
        $products = Product::all();

        return $products;
    }

    public function GetAllByUserId($user_id)
    {
        $products = Product::where("user_id", "=", $user_id)->get();

        return $products;
    }

    public function GetSingleById($id) : Product
    {
        $product = Product::where("id", "=", $id)->first();
        
        return $product == null ? new Product() : $product;
    }

    public function GetByCategoryId($category_id) 
    {
        return Product::where("category_id", $category_id)->get();
    }

}