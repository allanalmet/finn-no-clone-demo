<?php

namespace App\Services\Mock\Service;

use App\Models\Category;
use App\Models\CategoryType; //TODO temporary fix
use App\Services\Interfaces\CategoryServiceInterface;

use Illuminate\Support\Facades\Validator;

use App\Repositories\CategoryRepository;


class CategoryService implements CategoryServiceInterface
{

    public function __construct(CategoryRepository $categoryRepository) 
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function Store($data) : Category
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        $category = new Category();
        $category->name = $validate['name'];

        return $category;
    }

    public function remove($id){
        return $this->categoryRepository->remove($id);
    } 

    public function update($data) : Category
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        if ($validate->fails()) 
        {
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        $category = new Category();
        $category->name = $validate['name'];
        return $category;
    }


    public function GetCategoryById($id) 
    {
        return $this->categoryRepository->GetById($id);
    }

    public function GetSubTypesByParentId($category) 
    {
        return $this->categoryRepository->GetSubCategoriesById($category);
    }

    public function GetSubCategoriesByParentId($category) 
    {
        return $this->categoryRepository->GetTypesByCategoryId($category);
    }

    public function GetSubCategoryItemsByParentId($category, $id) 
    {
        return $this->categoryRepository->GetByCategoryId($id);
    }

    public function GetMainPageCategories() 
    {
        return $this->categoryRepository->GetMainPageCategories();
    }

    // NEW

    public function GetCategoryTypes($category_id) 
    {
        return (new CategoryType())->GetTypesByCategoryId($category_id);
    }
}
