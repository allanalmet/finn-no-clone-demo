<?php

namespace App\Services\Mock\Service;

use App\Models\History;
use App\Services\Interfaces\HistoryServiceInterface;
use Illuminate\Http\Request;

use App\Repositories\HistoryRepository;
use Illuminate\Support\Facades\Validator;

class HistoryService implements HistoryServiceInterface {

    public function __construct(HistoryRepository $historyRepository) 
    {
        $this->historyRepository = $historyRepository;
    }

    public function Store($data)
    {
        /*
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);
        */

        // 1) Check validation is true and false..
        if (true) 
        {
            throw new \InvalidArgumentException("test");
        }

        // 2) Check History Object..

        $history = new History();
        $history->id = $data['id'];
        $history->name = $data['name'];

        return $history;
    }

    public function Update($data)
    {
        $validate = Validator::make($data, [
            'name' => 'required|max:255',
            'id' => 'required',
        ]);

        // 1) Check validation is true and false..
        if ($validate->fails()) 
        {
            // returns InvalidArgumentException
            throw new \InvalidArgumentException($validate->errors()->first());
        }

        // 2) Check History Object..
        $history = new History();
        $history->id = $data['id'];
        $history->name = $data['name'];

        return $history;
    }

    public function GetSingleById($id)
    {
        $history = new History();
        $history->product_id = 1;
        $history->user_id = 1;
        return $history;
    }

    public function GetAll() 
    {
        $history1 = new History();
        $history1->product_id = 1;
        $history1->user_id = 1;

        $history2 = new History();
        $history2->product_id = 2;
        $history2->user_id = 1;

        $history3 = new History();
        $history3->product_id = 3;
        $history3->user_id = 1;

        return collect([$history1, $history2, $history3]);
    }
}
