<?php

namespace App\Repositories;

use App\Models\ProductTaxonomy;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;

class ProductTaxonomyRepository implements RepositoriesInterface
{
    

    public function __construct(ProductTaxonomy $productTaxonomy) 
    {
        $this->productTaxonomy = $productTaxonomy;
    }

    public function Save($data)
    {
        return empty($data["id"]) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->productTaxonomy->GetTable())
            ->insert([
                'name' => $data["name"],
                'title' => $data["title"]
            ]);
    }


    public function StoreAll($data)
    {
        return DB::table($this->productTaxonomy->GetTable())
            ->insert($data);
    }


    public function Update($data)
    {
        return DB::table($this->productTaxonomy->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"],
                'title' => $data["title"]
            ]);
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->productTaxonomy->GetTable())->get();
    }



    public function GetById($id)
    {
        return DB::table($this->productTaxonomy->GetTable())
            ->where('id', $id)
            ->first();
    }

    public function GetByProductId($id) 
    {
        return DB::table($this->productTaxonomy->GetTable())
            ->leftJoin("taxonomy", "taxonomy.id", "=", "product_taxonomies.taxonomy_id")
            ->where('product_taxonomies.product_id', $id)->get();
    }
}