<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Vehicle\VehicleDrive;
use App\Services\Interfaces\Repositories\RepositoriesInterface;

class VehicleDriveRepository implements RepositoriesInterface

{
    public function __construct(VehicleDrive $vehicleDrive) 
    {
        $this->vehicleDrive = $vehicleDrive;
    }



    public function Save($data)
    {
        return empty($data['id']) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->vehicleDrive->GetTable())
            ->insert([
                'name' => $data["name"]
            ]);
    }


    public function Update($data)
    {
        return DB::table($this->vehicleDrive->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"]
            ]);
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->vehicleDrive->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->vehicleDrive->GetTable())
        ->where('id', $id)
        ->first();

    }
}