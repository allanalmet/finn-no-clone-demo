<?php

namespace App\Repositories;

use App\Models\CategoryType;
use Illuminate\Support\Facades\DB;
use App\Services\Interfaces\Repositories\RepositoriesInterface;

class CategoryTypeRepository implements RepositoriesInterface 
{
    

    public function __construct(CategoryType $categoryType) 
    {
        $this->categoryType = $categoryType;
    }


    public function GetCategoryByParentId($parent_id) 
    {
    }



    public function GetByProductsCategoryId($category_id) 
    {
    }


    public function save($data) 
    {
    }



    public function remove($id){
    } 



    public function GetById($id) 
    {
    }



    public function GetSubCategoriesById($id) 
    {
    }



    public function GetMainPageCategories() 
    {
    }



    public function Store($data)
    {

    }



    public function Update($data)
    {

    }



    public function GetAll()
    {
    }


    public function GetTypesByCategoryId($category_id) 
    {
        return DB::table($this->categoryType->GetTable())
            ->where("category_types.category_id", $category_id)->get();
    }

    public function GetByCategoryId($category_id) 
    {
        return DB::table($this->categoryType->GetTable())
            ->where("category_types.id", $category_id)
            ->rightJoin("products", "products.category_type_id", "=", "category_types.id")
            ->rightJoin("product_images", "product_images.product_id", "=", "products.id")
            ->get();
    }

}