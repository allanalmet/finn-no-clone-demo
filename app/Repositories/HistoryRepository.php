<?php

namespace App\Repositories;

use App\Models\History;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;



class HistoryRepository implements RepositoriesInterface
{
    

    public function __construct(History $history) 
    {
        $this->history = $history;
    }


    public function Store($data)
    {

    }



    public function Update($data)
    {

    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->history->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->history->GetTable())
        ->where('id', $id)
        ->first();

    }



    public function Save($data)
    {

    }

}