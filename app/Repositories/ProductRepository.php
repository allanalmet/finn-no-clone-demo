<?php

namespace App\Repositories;

use App\Models\Product;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;



class ProductRepository implements RepositoriesInterface
{
    

    public function __construct(Product $product) 
    {
        $this->product = $product;
    }


    public function Store($data)
    {
        
        //TODO fix this shit
        $product = new Product();
        $product->name = $data["name"];
        $product->description = $data["description"];
        $product->price = $data["price"];
        $product->user_id = 1;
        $product->category_type_id = 1;

        $product->save();
        
        return $product;

    }



    public function Update($data)
    {
        $product = DB::table($this->product->GetTable())
            ->where('id', $data['id'])
            ->update([
                'name' => $data["name"],
                'description' => $data["description"],
                'price' => $data["price"],
            ]);

        return $product;
    }



    public function Remove($id)
    {

    }

    public function GetAll()
    {
        return DB::table($this->product->GetTable())->get();
    }

    public function GetById($id)
    {
        return DB::table($this->product->GetTable())
            ->join('product_images', 'products.id', '=', 'product_images.product_id')
            ->where('id', $id)
            ->first();

    }

    public function GetByCategoryTypeId($id) 
    {
        return DB::table($this->product->GetTable())
            ->join('product_images', 'products.id', '=', 'product_images.product_id')
            ->where('category_type_id', $id)
            ->get();
    }

    public function GetAllByName($name) 
    {
        return DB::table($this->product->GetTable())
            ->join('product_images', 'products.id', '=', 'product_images.product_id')
            ->where('name', 'like', "%" . $name . "%")
            ->get();
    }

    public function Save($data)
    {

    }

}