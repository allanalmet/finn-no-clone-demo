<?php

namespace App\Repositories;

use App\Models\ProductVehicleInfo;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;

class ProductVehicleInfoRepository implements RepositoriesInterface
{
    

    public function __construct(ProductVehicleInfo $productVehicleInfo) 
    {
        $this->productVehicleInfo = $productVehicleInfo;
    }


    public function Store($data)
    {
        $product = new ProductVehicleInfo();
        $product->product_id = $data["product_id"];
        $product->sale_types_id = $data["sale_type"];
        $product->product_car_colors_id = $data["car_color"];
        $product->product_car_drives_id = $data["car_drive"];
        $product->product_car_transmissions_id = $data["car_transmission"];
        $product->price = $data["price"];
        return $product->save();
    }

    public function StoreAll($data)
    {
        return DB::table($this->productVehicleInfo->GetTable())
            ->insert($data);
    }

    public function Update($data)
    {
        $product = DB::table($this->productVehicleInfo->GetTable())
            ->where('id', $data['id'])
            ->update([
                'name' => $data["name"],
                'description' => $data["description"],
                'price' => $data["price"],
            ]);

        return $product;
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->productVehicleInfo->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->productVehicleInfo->GetTable())
        ->where('id', $id)
        ->first();

    }



    public function Save($data)
    {

    }

}