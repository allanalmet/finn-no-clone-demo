<?php

namespace App\Repositories;

use App\Models\User;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;



class UserRepository implements RepositoriesInterface

{
    public function __construct(User $user) 
    {
        $this->user = $user;
    }



    public function Update($data) 
    {
        $this->user->save();
    }



    public function Store($data) 
    {
        $this->user->save();
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->user->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->user->GetTable())
        ->where('id', $id)
        ->first();

    }



    public function Save($data)
    {

    }

}