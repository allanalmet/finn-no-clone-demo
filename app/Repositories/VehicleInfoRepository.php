<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Vehicle\VehicleInfo;
use App\Services\Interfaces\Repositories\RepositoriesInterface;

class VehicleInfoRepository implements RepositoriesInterface

{
    public function __construct(VehicleInfo $vehicleInfo) 
    {
        $this->vehicleInfo = $vehicleInfo;
    }



    public function Save($data)
    {
        return empty($data['id']) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->vehicleInfo->GetTable())
            ->insert([
                'name' => $data["name"]
            ]);
    }


    public function Update($data)
    {
        return DB::table($this->vehicleInfo->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"]
            ]);
    }


    public function Remove($id)
    {

    }



    public function GetAll()
    {
        dd(DB::table($this->vehicleInfo->GetTable())
        ->get());
        return DB::table($this->vehicleInfo->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->vehicleInfo->GetTable())
        ->where('id', $id)
        ->first();

    }

}