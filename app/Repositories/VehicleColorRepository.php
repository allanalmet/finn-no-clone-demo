<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Vehicle\VehicleColor;

use App\Services\Interfaces\Repositories\RepositoriesInterface;

class VehicleColorRepository implements RepositoriesInterface
{
    public function __construct(VehicleColor $vehicleColor) 
    {
        $this->vehicleColor = $vehicleColor;
    }


    public function Save($data)
    {
        return empty($data['id']) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->vehicleColor->GetTable())
            ->insert([
                'name' => $data["name"]
            ]);
    }


    public function Update($data)
    {
        return DB::table($this->vehicleColor->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"]
            ]);
    }


    public function Remove($id)
    {

    }


    public function GetAll()
    {
        return DB::table($this->vehicleColor->GetTable())
            ->get();
    }



    public function GetById($id)
    {
        return DB::table($this->vehicleColor->GetTable())
            ->where('id', $id)
            ->first();
    }



}