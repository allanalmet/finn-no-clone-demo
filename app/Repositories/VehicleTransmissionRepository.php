<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Vehicle\VehicleTransmission;
use App\Services\Interfaces\Repositories\RepositoriesInterface;

class VehicleTransmissionRepository implements RepositoriesInterface

{
    public function __construct(VehicleTransmission $vehicleTransmission) 
    {
        $this->vehicleTransmission = $vehicleTransmission;
    }



    public function Save($data)
    {
        return empty($data['id']) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->vehicleTransmission->GetTable())
            ->insert([
                'name' => $data["name"]
            ]);
    }


    public function Update($data)
    {
        return DB::table($this->vehicleTransmission->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"]
            ]);
    }


    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->vehicleTransmission->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->vehicleTransmission->GetTable())
        ->where('id', $id)
        ->first();

    }

}