<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Services\Interfaces\Repositories\RepositoriesInterface;

class CategoryRepository implements RepositoriesInterface 
{
    

    public function __construct(Category $category) 
    {
        $this->category = $category;
    }


    public function GetTypesByCategoryId()
    {
        
    }



    public function GetCategoryByParentId($parent_id) 
    {
        return DB::table($this->category->GetTable())->where("parent_id", $parent_id)->get();
    }



    public function GetByProductsCategoryId($category_id) 
    {
        return DB::table($this->category->GetTable())
            ->where($this->category->GetTable() . ".id", $category_id)
            ->leftJoin("category_types", "category_types.category_id", "=", $this->category->GetTable() . ".id")
            ->leftJoin("products", "products.category_type_id", "=", "category_types.id")
            ->rightJoin("product_images", "product_images.product_id", "=", "products.id")
            ->get();
    }


    public function save($data) 
    {
        if (empty($data['id']))
        {
            $category = new $this->category;
            $category->name = $data['name'];
            $category->parent_id = $data['parentId'];
            $category->main_default = $data['mainDefault'];
            return $category->save(); // Lisamine
        } 
        else 
        {
            return DB::table($this->category->GetTable())
                ->where('id', $data['id'])
                ->update([
                    'name' => $data["name"],
                    'parent_id' => $data["parentId"],
                    'main_default' => $data["mainDefault"],
                ]);
        }

    }



    public function remove($id){
        return DB::table($this->category->GetTable())
            ->where("id", $id)
            ->delete();
    } 



    public function GetById($id) 
    {
        return DB::table($this->category->GetTable())
            ->where("id", $id)
            ->first();
    }



    public function GetSubCategoriesById($id) 
    {
        return DB::table($this->category->GetTable())
            ->where("parent_id", $id)->where("id", "!=", $id)->get();
    }



    public function GetMainPageCategories() 
    {
        return DB::table($this->category->GetTable())
            ->where("main_default", 1)->get();
    }



    public function Store($data)
    {

    }



    public function Update($data)
    {

    }



    public function GetAll()
    {
        return DB::table($this->category->GetTable())
            ->get();
    }
}