<?php

namespace App\Repositories;

use App\Models\SaleType;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;



class SaleTypeRepository implements RepositoriesInterface

{
    public function __construct(SaleType $saleType) 
    {
        $this->saleType = $saleType;
    }



    public function Save($data)
    {
        return empty($data['id']) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->saleType->GetTable())
            ->insert([
                'name' => $data["name"]
            ]);
    }


    public function Update($data)
    {
        return DB::table($this->saleType->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"]
            ]);
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->saleType->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->saleType->GetTable())
        ->where('id', $id)
        ->first();

    }

}