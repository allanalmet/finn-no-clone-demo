<?php

namespace App\Repositories;

use App\Models\ProductImage;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;

class ProductImagesRepository implements RepositoriesInterface
{
    

    public function __construct(ProductImage $productImage) 
    {
        $this->productImage = $productImage;
    }


    public function Store($data)
    {
        
        //TODO fix this shit
        $product = new Product();
        $product->name = $data["name"];
        $product->description = $data["description"];
        $product->price = $data["price"];
        $product->user_id = 1;
        $product->category_type_id = 1;

        $product->save();
        
        return $product;

    }

    public function StoreAll($data)
    {
        return DB::table($this->productImage->GetTable())
            ->insert($data);
    }

    public function Update($data)
    {
        $product = DB::table($this->productImage->GetTable())
            ->where('id', $data['id'])
            ->update([
                'name' => $data["name"],
                'description' => $data["description"],
                'price' => $data["price"],
            ]);

        return $product;
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->productImage->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->productImage->GetTable())
        ->where('id', $id)
        ->first();

    }



    public function Save($data)
    {

    }

}