<?php

namespace App\Repositories;

use App\Models\Settings;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;



class SettingsRepository implements RepositoriesInterface
{
    

    public function __construct(Settings $settings) 
    {
        $this->settings = $settings;
    }


    public function Store($data)
    {

    }



    public function Update($data)
    {

    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->settings->GetTable())
        ->get();

    }



    public function GetById($id)
    {
        return DB::table($this->settings->GetTable())
        ->where('id', $id)
        ->first();

    }



    public function Save($data)
    {

    }

}