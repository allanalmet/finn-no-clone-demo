<?php

namespace App\Repositories;

use App\Models\Taxonomy\Taxonomy;
use App\Services\Interfaces\Repositories\RepositoriesInterface;
use Illuminate\Support\Facades\DB;

class TaxonomyRepository implements RepositoriesInterface
{
    

    public function __construct(Taxonomy $taxonomy) 
    {
        $this->taxonomy = $taxonomy;
    }

    public function Save($data)
    {
        return empty($data["id"]) ? $this->Store($data) : $this->Update($data);
    }


    public function Store($data)
    {
        return DB::table($this->taxonomy->GetTable())
            ->insert([
                'name' => $data["name"],
                'title' => $data["title"]
            ]);
    }
    public function StoreAll($data)
    {
        return DB::table($this->taxonomy->GetTable())
            ->insert($data);
    }


    public function Update($data)
    {
        return DB::table($this->taxonomy->GetTable())
            ->where('id', $data["id"])
            ->update([
                'name' => $data["name"],
                'title' => $data["title"]
            ]);
    }



    public function Remove($id)
    {

    }



    public function GetAll()
    {
        return DB::table($this->taxonomy->GetTable())->get();
    }



    public function GetById($id)
    {
        return DB::table($this->taxonomy->GetTable())
            ->where('id', $id)
            ->first();
    }

    public function GetByParentId($id) 
    {
        return DB::table($this->taxonomy->GetTable())
            ->where('parent_id', $id)->get();
    }
}