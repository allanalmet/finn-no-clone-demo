<?php

namespace App\Providers;

use App\Services\Service\CategoryService;
use App\Services\Service\ProductService;
use App\Services\Service\HistoryService;
use App\Services\Service\SettingsService;
use App\Services\Service\TaxonomyService;
use App\Services\Service\SaleTypeService;
use App\Services\Service\VehicleTransmissionService;
use App\Services\Service\VehicleDriveService;
use App\Services\Service\VehicleColorService;
use App\Services\Service\VehicleInfoService;
use App\Services\Service\ProductTaxonomyService;
use App\Services\Service\ProductImagesService;
use App\Services\Service\SearchService;
use App\Services\Service\ProductVehicleInfoService;
use App\Services\Test\CategoryServiceTests;
use App\Services\Test\ProductServiceTests;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Services\Interfaces\CategoryServiceInterface;
use App\Services\Interfaces\ProductServiceInterface;
use App\Services\Interfaces\HistoryServiceInterface;
use App\Services\Interfaces\SettingsServiceInterface;
use App\Services\Interfaces\TaxonomyServiceInterface;
use App\Services\Interfaces\SaleTypeServiceInterface;
use App\Services\Interfaces\VehicleTransmissionServiceInterface;
use App\Services\Interfaces\VehicleDriveServiceInterface;
use App\Services\Interfaces\VehicleColorServiceInterface;
use App\Services\Interfaces\VehicleInfoServiceInterface;
use App\Services\Interfaces\ProductTaxonomyServiceInterface;
use App\Services\Interfaces\ProductImagesServiceInterface;
use App\Services\Interfaces\SearchServiceInterface;
use App\Services\Interfaces\ProductVehicleInfoServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (env("APP_TESTING"))
        {
            $this->app->bind(CategoryServiceInterface::class, CategoryServiceTests::class);
            $this->app->bind(ProductServiceInterface::class, ProductServiceTests::class);
        }
        else
        {
            $this->app->bind(CategoryServiceInterface::class, CategoryService::class);
            $this->app->bind(ProductServiceInterface::class, ProductService::class);
            $this->app->bind(SettingsServiceInterface::class, SettingsService::class);
            $this->app->bind(HistoryServiceInterface::class, HistoryService::class);
            $this->app->bind(TaxonomyServiceInterface::class, TaxonomyService::class);
            $this->app->bind(ProductTaxonomyServiceInterface::class, ProductTaxonomyService::class);
            $this->app->bind(SaleTypeServiceInterface::class, SaleTypeService::class);
            $this->app->bind(VehicleTransmissionServiceInterface::class, VehicleTransmissionService::class);
            $this->app->bind(VehicleDriveServiceInterface::class, VehicleDriveService::class);
            $this->app->bind(VehicleColorServiceInterface::class, VehicleColorService::class);
            $this->app->bind(VehicleInfoServiceInterface::class, VehicleInfoService::class);
            $this->app->bind(ProductTaxonomyServiceInterface::class, ProductTaxonomyService::class);
            $this->app->bind(ProductImagesServiceInterface::class, ProductImagesService::class);
            $this->app->bind(SearchServiceInterface::class, SearchService::class);
            $this->app->bind(ProductVehicleInfoServiceInterface::class, ProductVehicleInfoService::class);
        }

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
