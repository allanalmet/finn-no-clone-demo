<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\CategoryServiceInterface;

class CategoriesController extends Controller
{

    private $_categoryService;

    public function __construct(CategoryServiceInterface $categoryService) 
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("components.main.categories");
    }

    public function GetCategoriesData()
    {
        return response()->json([
            "categories" => $this->_categoryService->GetAll()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $sub
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        return view("components.main.category");
    }

    /*
    public function GetCategoriesShowData($category) 
    {
        return response()->json([
            "sub_categories" => $this->_categoryService->GetSubTypesByParentId($category),
            "parent_category" => $this->_categoryService->GetCategoryById($category)

        ]);
    }
    */

    public function SubCategory($category) 
    {
        return view("components.main.category_items");
    }

    public function GetSubCategoryData($category, $id) 
    {
        return response()->json([
            "sub_categories_items" => $this->_categoryService->GetSubCategoriesByParentId($id)
        ]);
    }



    public function GetProductsByCategoryId($category_id) 
    {        
        return response()->json([
            'category_products' => $this->_categoryService->GetByProductsCategoryId($category_id)
        ]);
    }
}
