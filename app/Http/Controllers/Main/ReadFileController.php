<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Taxonomy\Taxonomy;
use Illuminate\Support\Facades\Storage;
use App\Services\Interfaces\TaxonomyServiceInterface;

class ReadFileController extends Controller
{

    public function __construct(TaxonomyServiceInterface $taxonomyService) 
    {
        $this->taxonomyService = $taxonomyService;
    }

    public function index()
    {
        $dd=$this->taxonomyService->GetAll();
        if(count($dd)>0) return;
        $contents = Storage::get("varustus-list.txt");

        $splittedContents = preg_split('/\r\n|\r|\n/', $contents);
        $taxonomies = [];
        foreach($splittedContents as $line) {
            if(empty($line)) continue;
            $taxonomy = [
                "name" => trim($line),
                "title" => 0,
                "parent_id" => 2,
                
            ];

            if (strpos($line, '<h3>') !== false) {
                $taxonomy["title"] = 1;
            }
            $taxonomies[] = $taxonomy;
            // dd($taxonomy);

        }
        $Tdata = $this->taxonomyService->StoreAll($taxonomies); 
        dd($Tdata);
        

    }
}
