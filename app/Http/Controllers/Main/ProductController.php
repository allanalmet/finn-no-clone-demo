<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ViewModels\Api\ProductViewModels;
use App\Services\Interfaces\ProductServiceInterface;

class ProductController extends Controller
{

    private $_productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->_productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        return view("components.main.product_detail");
    }

    //
    public function GetProductData($id)
    {
        $product = $this->_productService->GetSingleById($id);        
        return response()->json([
            "product" => $product
        ]);
    }




}
