<?php

// namespace App\Http\Controllers\Trader;

// use App\Models\Product;
// use Illuminate\Http\Request;
// use App\Services\ProductService;
// use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
// use App\Models\ViewModels\ProductViewModels;
// use App\Services\Interfaces\ProductServiceInterface;

// class ProductController extends Controller
// {

//     private $_productService;

//     public function __construct(ProductServiceInterface $productService)
//     {
//         $this->_productService = $productService;
//     }

//     /**
//      * Display a listing of the resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
//     public function index()
//     {
//         $userProducts = $this->_productService->GetAllByUserId(Auth::user()->id);
//         dd($userProducts);

//         $products = [];
//         foreach($userProducts as $p){
//             $products[] = new ProductViewModels($p->id, $p->name, $p->description, $p->category_id, $p->price);
//         }
//         //dd($products);

//         return view("components.trader.products", [
//             "products" => $products
//         ]);

//     }

//     /**
//      * Show the form for creating a new resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
//     public function create()
//     {
//         //
//     }

//     /**
//      * Store a newly created resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return \Illuminate\Http\Response
//      */
//     public function store(Request $request)
//     {
//         //$excistingCategory = $productService->GetSingleByName($request->Product_name); 
//         //dd($excistingCategory);
//         //if ($excistingCategory->id > 0) return redirect()->action([ProductController::class, 'index']);
//         $product = $this->_productService->Store($request);
//         return $product->id > 0 ? redirect()->action([ProductController::class, 'index']) : "failed";
//     }

//     /**
//      * Display the specified resource.
//      *
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function show($id)
//     {
//         //
//     }

//     /**
//      * Show the form for editing the specified resource.
//      *
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function edit($id)
//     {
//         //
//     }

//     /**
//      * Update the specified resource in storage.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function update(Request $request, $id)
//     {
//         //
//     }

//     /**
//      * Remove the specified resource from storage.
//      *
//      * @param  int  $id
//      * @return \Illuminate\Http\Response
//      */
//     public function destroy($id)
//     {
//         //
//     }
// }