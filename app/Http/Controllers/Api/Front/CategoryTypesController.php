<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\CategoryServiceInterface;

class CategoryTypesController extends Controller
{

    private $_categoryService;

    public function __construct(CategoryServiceInterface $categoryService) 
    {
        $this->_categoryService = $categoryService;
    }

    public function GetCategoryTypes($id) 
    {
        return response()->json([
            "category_types" => $this->_categoryService->GetSubTypesByParentId($id)
        ]);
    }

}