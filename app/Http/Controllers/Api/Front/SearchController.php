<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\SearchServiceInterface;
use Exception;

class SearchController extends Controller
{
    private $_searchService;

    public function __construct(SearchServiceInterface $searchService)
    {
        $this->_searchService = $searchService;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(string $name)
    {
        $result = ['status' => 200];

        try{
            $result['data'] = $this->_searchService->GetAllByName(["name" => $name]);
        } catch(Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

}