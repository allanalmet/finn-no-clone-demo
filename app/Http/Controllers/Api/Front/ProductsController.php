<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\ProductServiceInterface;
use Exception;

class ProductsController extends Controller
{
    private $_productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->_productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "products" => $this->_productService->GetAll()
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            "products" => $this->_productService->GetSingleById($id)
        ]);
    }

    public function GetProductsByCategoryId($category_id) 
    {
        $products = $this->_productService->GetByParentCategoryId($category_id);

        return response()->json([
            "products" => $products
        ]);
    }
}
