<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\TaxonomyServiceInterface;
use App\Services\Interfaces\ProductTaxonomyServiceInterface;
use Exception;

class TaxonomyController extends Controller
{
    private $_taxonomyService;
    private $_productTaxonomyService;

    public function __construct(
        TaxonomyServiceInterface $taxonomyService, ProductTaxonomyServiceInterface $productTaxonomyService)
    {
        $this->_taxonomyService = $taxonomyService;
        $this->_productTaxonomyService = $productTaxonomyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "taxonomies" => $this->_taxonomyService->GetAll()
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        return response()->json([
            "taxonomies" => $this->_productTaxonomyService->GetByProductId($product_id)
        ]);
    }
}
