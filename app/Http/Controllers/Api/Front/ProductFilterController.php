<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\ProductServiceInterface;
use Exception;

class ProductFilterController extends Controller
{
    private $_productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->_productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "products" => $this->_productService->GetAll()
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            "products" => $this->_productService->GetSingleById($id)
        ]);
    }

    public function GetProductsByCategoryTypeId($category_id) 
    {
        return response()->json([
            "products" => $this->_productService->GetByCategoryTypeId($category_id)
        ]);
    }
}
