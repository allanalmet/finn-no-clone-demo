<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Route;

class RoutesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "routes" => [
                "categories" => "/admin/categories/index",
                "products" => "/admin/products/index",
                "settings" => "/admin/settings/index",
            ]
        ]);
    }
}
