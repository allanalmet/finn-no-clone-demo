<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\VehicleInfoServiceInterface;
use Exception;

class VehicleController extends Controller
{
    private $_vehicleInfoService;

    public function __construct(VehicleInfoServiceInterface $vehicleInfoService)
    {
        $this->_vehicleInfoService = $vehicleInfoService;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        return response()->json([
            "vehicle_info" => $this->_vehicleInfoService->GetVehicleInfo($product_id)
        ]);
    }
}
