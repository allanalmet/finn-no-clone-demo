<?php

namespace App\Http\Controllers\Api\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\ProductServiceInterface;
use App\Services\Interfaces\ProductImagesServiceInterface;
use App\Services\Interfaces\ProductTaxonomyServiceInterface;
use App\Services\Interfaces\ProductVehicleInfoServiceInterface;

use Illuminate\Support\Facades\DB;

use Exception;

class ProductsController extends Controller
{
    private $_productService;
    private $_productImagesService;
    private $_productTaxonomyService;
    private $_productVehicleInfoService;

    public function __construct(
        ProductVehicleInfoServiceInterface $productVehicleService,
        ProductServiceInterface $productService, 
        ProductImagesServiceInterface $productImagesService, 
        ProductTaxonomyServiceInterface $productTaxonomyService
    )
    {
        $this->_productVehicleInfoService = $productVehicleService;
        $this->_productService = $productService;
        $this->_productImagesService = $productImagesService;
        $this->_productTaxonomyService = $productTaxonomyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            "products" => $this->_productService->GetAll()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'images',
            'name',
            'description',
            'price',
            'location',
            'taxonomies',
            'car_color',
            'car_drive',
            'car_transmission',
            'sale_type',
        ]);

        $result = ['status' => 200];

        try{
            DB::transaction(function () use ($data, $request) {
                $product = $this->_productService->Store($data);
                $data["product_id"] = $product->id;
                $productInfoService = $this->_productVehicleInfoService->Store($data);
                $taxonomy = $this->_productTaxonomyService->StoreAll($data);
                $result['data'] = [
                    "products" => $product,
                    "taxonomy" => $taxonomy,
                ];
                if($data["images"] != null) 
                {
                    $productImages = $this->_productImagesService->StoreAll($data);
                    $result["data"]["product_images"] = $productImages;
                }                
            });
            

        } catch(Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage(),
            ];
        }

        return response()->json($result, $result['status']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            "products" => $this->_productService->GetSingleById($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json([
            "products" => $this->_productService->GetSingleById($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'name',
            'description',
            'price',
            'location',
        ]);

        $data['id'] = $id;
        $result = ['status' => 200];

        try{
            $result['data'] = $this->_productService->Update($data);
        } catch(Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
