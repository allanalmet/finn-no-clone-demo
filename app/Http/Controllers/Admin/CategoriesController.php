<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ViewModels\CategoryViewModels;
use App\Services\Interfaces\CategoryServiceInterface;

class CategoriesController extends Controller
{
    private $_categoryService;

    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("components.admin.categories.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("components.admin.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $data = $request->only([
            'name',
        ]);

        $result = ['status' => 200];

        try {
            $result['data'] = $this->_categoryService->Store($data);
        } catch (\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        return view("components.admin.categories.show");

    }


    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        return view("components.admin.categories.edit");

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(Request $request, $id)
    {
        $data = $request->only([
            'name',
        ]);

        $data['id'] = $id;

        $result = ['status' => 200];

        try {
            $result['data'] = $this->_categoryService->update($data);
        } catch (\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function destroy($id)
    {
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_categoryService->remove($id);
        } catch (\Exception $e){
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);

    }
}


        // $excistingCategory = $this->_categoryService->GetSingleByName($request->category_name); 
        //dd($excistingCategory);
        // if ($excistingCategory->id > 0) return redirect()->action([CategoriesController::class, 'index']);
        // $category = $this->_categoryService->Store($request);
        // return $category->id > 0 ? redirect()->action([CategoriesController::class, 'index']) : "failed";
