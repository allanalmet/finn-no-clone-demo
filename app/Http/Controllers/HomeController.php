<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ViewModels\UserViewModels;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$dd = new UserViewModels(Auth::user()->name, Auth::user()->email);
        //dd($dd);
        //$user = new User();
        //return view('home', [
        //    "user" => $dd
        //]);

        return view("home");

    }
}
