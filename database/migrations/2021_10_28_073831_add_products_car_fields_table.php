<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductsCarFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_car', function (Blueprint $table) {
            $table->integer("private");
            $table->integer("sale_types_id");
            $table->integer("year_model");
            $table->integer("milage");
            $table->string("location");
            $table->integer("seats");
            $table->integer("product_car_colors_id");
            $table->integer("product_car_drives_id");
            $table->integer("product_car_transmissions_id");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_car');
    }
}
