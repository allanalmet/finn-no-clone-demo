<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_car', function (Blueprint $table) {
            $table->id();
            $table->integer("product_id");
            $table->integer("body_style_id"); // TODO eraldi tabel juurde
            $table->integer("model_year");
            $table->integer("price");
            $table->integer("power");
            $table->integer("fuel_type_id"); // TODO eraldi tabel juurde
            $table->integer("transmission_id");
            $table->integer("drive_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_car_filter');
    }
}
