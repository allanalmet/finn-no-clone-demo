<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class ProductImagesSeeder extends Seeder
{

    protected $table = "product_images";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 1,
            'product_id' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 1,
            'product_id' => 2,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 2,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 2,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 1,
            'product_id' => 3,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 3,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'image' => "510.jpg",
            'default' => 0,
            'product_id' => 3,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
