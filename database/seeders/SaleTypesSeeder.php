<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class SaleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('sale_types')->insert([
            'name' => "renting",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('sale_types')->insert([
            'name' => "leasing",
            'created_at' => $now,
            'updated_at' => $now,
        ]);        
        
        DB::table('sale_types')->insert([
            'name' => "new",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        
        DB::table('sale_types')->insert([
            'name' => "used",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

    }
}
