<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();

        DB::table('products')->insert([
            'name' => "BMW X5",
            'description' => "X5",
            'category_type_id' => 1,
            'user_id' => 1,
            'price' => 4500,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('products')->insert([
            'name' => "BMW X3",
            'description' => "X3",
            'category_type_id' => 2,
            'user_id' => 1,
            'price' => 5500,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('products')->insert([
            'name' => "BMW X6",
            'description' => "X6",
            'category_type_id' => 3,
            'user_id' => 1,
            'price' => 3300,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('products')->insert([
            'name' => "BMW 316",
            'description' => "316",
            'category_type_id' => 4,
            'user_id' => 1,
            'price' => 4500,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('products')->insert([
            'name' => "BMW 316",
            'description' => "316",
            'category_type_id' => 4,
            'user_id' => 1,
            'price' => 5500,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('products')->insert([
            'name' => "BMW X6",
            'description' => "X6",
            'category_type_id' => 3,
            'user_id' => 1,
            'price' => 3300,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        
    }
}