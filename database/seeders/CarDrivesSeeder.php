<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class CarDrivesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('product_car_transmissions')->insert([
            'name' => "automatic",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('product_car_transmissions')->insert([
            'name' => "manual",
            'created_at' => $now,
            'updated_at' => $now,
        ]);        
    }
}
