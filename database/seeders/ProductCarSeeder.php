<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/* Not used */

class ProductCarSeeder extends Seeder
{

    protected $table = "products_car";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Not used */
        DB::table($this->table)->insert([
            'category_id' => 1
        ]);
        DB::table($this->table)->insert([
            'category_id' => 2
        ]);
        DB::table($this->table)->insert([
            'category_id' => 1
        ]);
        DB::table($this->table)->insert([
            'category_id' => 2
        ]);
    }
}
