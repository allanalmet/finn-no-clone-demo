<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
/*
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(CategoryTypesSeeder::class);
        $this->call(ProductImagesSeeder::class);
        
        $this->call(HistoryTableSeeder::class);
        $this->call(SettingsTableSeeder::class);

*/        

        $this->call(CarColorSeeder::class);
        $this->call(CarDrivesSeeder::class);
        $this->call(CarTransmissionsSeeder::class);
        $this->call(SaleTypesSeeder::class);

    }
}
