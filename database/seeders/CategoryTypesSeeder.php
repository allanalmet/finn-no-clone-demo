<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

Use \Carbon\Carbon;

class CategoryTypesSeeder extends Seeder
{

    protected $table = "category_types";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table($this->table)->insert([
            'type_label' => "X5",
            'category_id' => 14,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'type_label' => "X3",
            'category_id' => 14,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'type_label' => "X6",
            'category_id' => 14,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'type_label' => "316",
            'category_id' => 14,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
