<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class CarTransmissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('product_car_drives')->insert([
            'name' => "4WD",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('product_car_drives')->insert([
            'name' => "FWD",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('product_car_drives')->insert([
            'name' => "RWD",
            'created_at' => $now,
            'updated_at' => $now,
        ]);        
    }
}
