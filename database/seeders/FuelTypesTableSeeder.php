<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class FuelTypesTableSeeder extends Seeder
{

    protected $table = "fuel_types";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table($this->table)->insert([
            'name' => "bensiin",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "diisel",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "bensiin + gaas (LPG/vedelgaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "bensiin + gaas (CNG/surugaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "bensiin + gaas (LNG/veeldatud maagaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "diisel + gaas (LNG/veeldatud maagaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "gaas (LPG/vedelgaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "gaas (CNG/surugaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "gaas (LNG/veeldatud maagaas)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "hübriid",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "hübriid (bensiin / elekter)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "hübriid (diisel / elekter)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "pistikhübriid (bensiin / elekter)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "pistikhübriid (diisel / elekter)",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "elekter",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        DB::table($this->table)->insert([
            'name' => "etanool",
            'created_at' => $now,
            'updated_at' => $now,            
        ]);


    }
}


