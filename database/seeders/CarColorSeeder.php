<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

Use \Carbon\Carbon;

class CarColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('product_car_colors')->insert([
            'name' => "white",
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('product_car_colors')->insert([
            'name' => "black",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        

    }
}
