<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoryTableSeeder extends Seeder
{

    protected $table = "history";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert([
            'product_id' => 1,
            'user_id' => 1,
        ]);
        DB::table($this->table)->insert([
            'product_id' => 2,
            'user_id' => 1,
        ]);
    }
}
