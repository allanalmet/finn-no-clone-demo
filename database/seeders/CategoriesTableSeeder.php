<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;


class CategoriesTableSeeder extends Seeder
{

    protected $table = "categories";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();

        DB::table($this->table)->insert([
            'name' => "Turg",
            'parent_id' => 1,
            "main_default" => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Transport",
            'parent_id' => 2,
            "main_default" => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Tööpakkumised",
            'parent_id' => 3,
            "main_default" => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Kinnisvara",
            'parent_id' => 4,
            "main_default" => 1,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Oksjon",
            'parent_id' => 5,
            "main_default" => 1,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

        // DB::table($this->table)->insert([
        //     'name' => "Laud",
        //     'parent_id' => 1,
        //     "main_default" => 0,
        //     'created_at' => $now,
        //     'updated_at' => $now,            
        // ]);
        // DB::table($this->table)->insert([
        //     'name' => "Kapp",
        //     'parent_id' => 1,
        //     "main_default" => 0,
        //     'created_at' => $now,
        //     'updated_at' => $now,            
        // ]);
        // DB::table($this->table)->insert([
        //     'name' => "Lamp",
        //     'parent_id' => 1,
        //     "main_default" => 0,
        //     'created_at' => $now,
        //     'updated_at' => $now,            
        // ]);
        // DB::table($this->table)->insert([
        //     'name' => "Voodi",
        //     'parent_id' => 1,
        //     "main_default" => 0,
        //     'created_at' => $now,
        //     'updated_at' => $now,            
        // ]);

        DB::table($this->table)->insert([
            'name' => "Sõiduauto",
            'parent_id' => 2,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Maastur",
            'parent_id' => 2,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Veoauto",
            'parent_id' => 2,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'name' => "Audi",
            'parent_id' => 10,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "BMW",
            'parent_id' => 10,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Ford",
            'parent_id' => 10,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "VW",
            'parent_id' => 10,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table($this->table)->insert([
            'name' => "Front-End arendaja",
            'parent_id' => 3,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Back-End arendaja",
            'parent_id' => 3,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "Devops",
            'parent_id' => 3,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($this->table)->insert([
            'name' => "UI/UX",
            'parent_id' => 3,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);

    }
}
