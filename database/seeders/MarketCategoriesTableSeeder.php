<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;


class MarketCategoriesTableSeeder extends Seeder
{

    protected $table = "categories";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();


    // TURG ALA KATEGOORIAD

        DB::table($this->table)->insert([
            'name' => "Aed, kodu ja renoveerimine",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Antiik ja Kunst",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Elektroonika ja kodutehnika",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Kosmeetika",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Lastetarbed",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Loomad ja varustus",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Meelelahutus ja hobid",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Mööbel ja sisustus",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Riided ja aksessuaarid",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Sport ja vabaaeg",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Teenused",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Tööstus ja tootmine",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
        DB::table($this->table)->insert([
            'name' => "Varuosad",
            'parent_id' => 1,
            "main_default" => 0,
            'created_at' => $now,
            'updated_at' => $now,            
        ]);
    }
}