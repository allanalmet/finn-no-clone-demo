import React, { Component } from "react";
import { categoryTypes } from "../../constants";

export default class FrontPublishedItem extends Component {

    constructor(props) {
        super(props);

        /* Example 1 */
        const { publishMain, sidebarFilterMain } = this.props;

        /*
            Example 2
            this.state = {
                publish: this.props.publishMain,
                sidebarFilter: this.props.sidebarFilterMain
            }

        */

        this.state = {
            sidebarFilter: sidebarFilterMain,
            data: {
                publish: publishMain,
                type: categoryTypes.published,
            },
        }
    }

    render() {
        return (
            <div className="col-md-12">

                <div className="row">
                    <div className="col-md-2">
                        <input type="checkbox" onChange={ev => this.state.sidebarFilter(ev, this.state.data)} />
                    </div>
                    <div className="col-md-8">
                        {this.state.data.publish.name}({this.state.data.publish.count})
                    </div>
                </div>

            </div>
        )
    }
}
