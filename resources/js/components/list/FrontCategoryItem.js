import { ConstructionOutlined } from "@mui/icons-material";
import { identity } from "lodash";
import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { categoryTypes } from "../../constants";

export default class FrontCategoryItem extends Component {

    constructor(props) {
        super(props);

        this.state = {            
            selectedCategory: this.props.selectedCategory,
            sidebarFilter: this.props.sidebarFilter,
            style: {
                marginLeft: {
                    "marginLeft": "10px"
                } 
            },
            data: {
                category: this.props.category,
                type: categoryTypes.category,
            },
            toggled: false,
        };

        console.log("category_types -> ", this.props.category_types);
    }

    buildSubTypes = () => 
    {
        console.log("buildSubTypes 2 -> ");
        console.log("category_types -> ", this.props.category_types);
        const categoryTypes = this.props.category_types.map(item => {
            console.log("this.props.category_types.data -> ", item);
            return (
                <div key={item.id} className="row">
                    <div className="col-md-2">
                        <input type="checkbox" 
                            onChange={ev => {
                                this.state.sidebarFilter(ev, item, true, true)
                            }}
                        />
                    </div>
                    <div className="col-md-8">
                        {item.type_label}
                    </div>
                </div>
            );
        })
        
        return (
            <div style={{ "marginLeft": "40px" }}>
                {
                    categoryTypes
                }
            </div>
        )
    }

    render() {
        return (
            <div>

                <div style={this.state.data.category.id != this.state.data.category.parentId ? this.state.style.marginLeft : null} className="row">
                    <div className="col-md-2">
                        <input type="checkbox" onChange={ev => {
                            this.setState(prevState => ({
                                toggled: !this.state.toggled
                            }));
                            this.state.sidebarFilter(ev, this.state.data, this.state.toggled)
                        }} />
                    </div>
                    <div className="col-md-8">
                        {this.state.data.category.name}
                    </div>
                    
                </div>

                {this.props.category_types != undefined ? this.buildSubTypes() : null }
            </div>
        );
    }

}
