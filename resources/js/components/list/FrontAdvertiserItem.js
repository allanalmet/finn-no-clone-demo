import React, { Component } from "react";
import { categoryTypes } from "../../constants";

export default class FrontAdvertiserItem extends Component {

    constructor(props) {
        super(props);

        /* Example 1 */
        const { advertiserMain, sidebarFilterMain } = this.props;

        /*
            Example 2
            this.state = {
                publish: this.props.publishMain,
                sidebarFilter: this.props.sidebarFilterMain
            }

        */

        this.state = {            
            sidebarFilter: sidebarFilterMain,
            data: {
                advertiser: advertiserMain,
                type: categoryTypes.advertiser,
            }
        }
    }

    render() {
        return (
            <div className="col-md-12">

                <div className="row">
                    <div className="col-md-2">
                        <input type="checkbox" onChange={ev => this.state.sidebarFilter(ev, this.state.data)} />
                    </div>
                    <div className="col-md-8">
                        {this.state.data.advertiser.name}({this.state.data.advertiser.count})
                    </div>
                </div>

            </div>
        )
    }
}
