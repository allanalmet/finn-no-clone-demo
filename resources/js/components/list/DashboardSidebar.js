import React, { Component } from 'react';

class DashboardSidebar extends Component {


    constructor(props) {
        super(props);
    }

    buildList = () => {
        return this.props.sidebar.map((item, index) => {
            return (
                <li key={index} className="c-sidebar-nav-item">
                    <a className="c-sidebar-nav-link" href={item.url}>
                        {item.label}
                    </a>
                </li>
            );
        });
    }

    render() {
        return (
            <ul>
                {this.buildList()}
            </ul>
        );
    }
}

export default DashboardSidebar;