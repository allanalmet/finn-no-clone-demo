import React, { Component } from "react";
import { categoryTypes } from "../../constants";

export default class FrontFormOfSaleItem extends Component {

    constructor(props) {
        super(props);

        /* Example 1 */
        const { formOfSaleMain, sidebarFilterMain } = this.props;

        /*
            Example 2
            this.state = {
                publish: this.props.publishMain,
                sidebarFilter: this.props.sidebarFilterMain
            }

        */

        this.state = {
            sidebarFilter: sidebarFilterMain,
            data: {
                formOfSale: formOfSaleMain,
                type: categoryTypes.formOfSale
            }
        }
    }

    render() {
        return (
            <div className="col-md-12">

                <div className="row">
                    <div className="col-md-2">
                        <input type="checkbox" onChange={ev => this.state.sidebarFilter(ev, this.state.data)} />
                    </div>
                    <div className="col-md-8">
                        {this.state.data.formOfSale.name}({this.state.data.formOfSale.count})
                    </div>
                </div>

            </div>
        )
    }
}
