import React, { Component } from "react";
import ReactDOM from 'react-dom';
import ImageLoader from "react-loading-image";

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';

import "./CategoryProductItem.css";

export default class CategoryProductItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            product: this.props.product,
            style: {
                "paddingTop": "10px",
                "paddingBottom": "10px",
            },
            checkBoxIsChecked: false

        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(ev){
        ev.preventDefault();
        this.setState(prevState=> ({checkBoxIsChecked: !prevState.checkBoxIsChecked}));
    }

    render() {

        const image = "/img/" + this.props.product.image;
        const detailPage = "/product/detail/" + this.props.product.id;

        return (
            <Card className="item-box">
                <a className="item-box-anchor" href={detailPage}>
                    <div className="item-box-image">
                        <CardMedia
                            component="img"
                            image={image}
                            alt={this.props.product.name}
                        />
                    </div>
                    <div className="item-box-text">
                        <div>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {this.props.product.name}
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <div>{this.props.product.price}</div>
                                    <div>{this.props.product.location}</div>
                                </Typography>
                            </CardContent>
                        </div>
                        <div className="item-box-icon">
                            <CardContent>
                                {this.state.checkBoxIsChecked 
                                    ? <CheckBoxIcon onClick={this.handleClick} style={{fill: "#2F8D37"}} fontSize="large"></CheckBoxIcon>  
                                    : <CheckBoxOutlineBlankIcon onClick={this.handleClick} style={{fill: ""}} fontSize="large"></CheckBoxOutlineBlankIcon>
                                }
                            </CardContent>    
                        </div>
                    </div>
                </a>
            </Card>
        );
    }

}
