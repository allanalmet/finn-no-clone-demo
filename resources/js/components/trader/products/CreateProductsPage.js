import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import TraderBaseComponent from './../TraderBaseComponent';

export default class CreateProductsPage extends TraderBaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            description: null,
            location: null,
            price: 0,
        }
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("description", this.state.description);
        formData.append("location", this.state.location);
        formData.append("price", this.state.price);


        axios({
            method: 'post',
            url: "/api/trader/products/index",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState(prevState => ({
                name: null,
                description: null,
                location: null,
                price: 0,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div class="card">
                            <div class="card-header"><strong>Create Product</strong> </div>
                            <div class="card-body">
                                <form class="form-horizontal" action="" method="post">
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="product-name">Name:</label>
                                        <div class="col-md-9">
                                            <input 
                                                class="form-control" 
                                                id="product-name" 
                                                value={this.state.name} 
                                                name="name" 
                                                type="text" 
                                                placeholder="Enter Name.."
                                                onChange={ev => this.changeValue(ev)} 
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="product-description">Description:</label>
                                        <div class="col-md-9">
                                            <textarea 
                                                class="form-control" 
                                                name="description" 
                                                id="product-description"
                                                onChange={ev => this.changeValue(ev)}
                                            >
                                                {this.state.description}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="product-location">Location:</label>
                                        <div class="col-md-9">
                                            <input 
                                                class="form-control" 
                                                id="product-location" 
                                                name="location" 
                                                value={this.state.location} 
                                                type="text" 
                                                placeholder="Enter Location.."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="product-price">Price:</label>
                                        <div class="col-md-9">
                                            <input 
                                                class="form-control" 
                                                id="product-price" 
                                                name="price" 
                                                value={this.state.price} 
                                                type="text" 
                                                placeholder="Enter Price.."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit">
                                    Insert
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('trader-products-create')) {
    ReactDOM.render(<CreateProductsPage />, document.getElementById('trader-products-create'));
}
