import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import TraderBaseComponent from './../TraderBaseComponent';

export default class ShowProductsPage extends TraderBaseComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

            </CoreUiLayout>
        )
    }
}
if (document.getElementById('trader-products-show')) {
    ReactDOM.render(<ShowProductsPage />, document.getElementById('trader-products-show'));
}
