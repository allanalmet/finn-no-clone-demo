import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import TraderBaseComponent from './../TraderBaseComponent';

import "./IndexCategoriesPage.css";

export default class IndexProductsPage extends TraderBaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            products: null
        };
    }

    openCreatePage() {
        window.location.href = "/trader/products/create";
    }

    componentDidMount = () => {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/trader/products/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .then(function (response) {
                // handle success
                console.log("response -> ", response.data.products);
                $this.setState(prevState => ({
                    products: response.data.products,
                }));
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    buildProducts = () => {
        var products = this.state.products.map((item, index) => {
            const hrefUrl = "/trader/products/edit/" + item.id;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <a href={hrefUrl} class="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div class="card">
                <div class="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i>
                        Products
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary admin-panel-add-button" onClick={ev => this.openCreatePage()}>
                            Lisa
                        </button>
                    </div>
                </div>
                <div class="card-body">

                    <table class="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {products}
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {
                            this.state.products != null ? this.buildProducts() : null
                        }
                    </div>
                </div>

            </CoreUiLayout>
        )
    }
}
if (document.getElementById('trader-products-index')) {
    ReactDOM.render(<IndexProductsPage />, document.getElementById('trader-products-index'));
}
