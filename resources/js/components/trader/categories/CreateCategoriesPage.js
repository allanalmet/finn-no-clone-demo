import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import TraderBaseComponent from './../TraderBaseComponent';

export default class CreateCategoriesPage extends TraderBaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            parentId: "",
            mainDefault: "",
        }
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("parentId", this.state.parentId);
        formData.append("mainDefault", this.state.mainDefault);


        axios({
            method: 'post',
            url: "/api/trader/categories/index",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState(prevState => ({
                name: null,
                parentId: null,
                mainDefault: null,
                }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }



    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div class="card">
                            <div class="card-header"><strong>Create Category</strong> </div>
                            <div class="card-body">
                                <form class="form-horizontal" action="" method="post">
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="category-name">Category Name:</label>
                                        <div class="col-md-9">
                                            <input 
                                            class="form-control" 
                                            id="category-name" 
                                            type="text" 
                                            name="name" 
                                            placeholder="Enter Category ..." 
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="category-parent-id">Parent ID:</label>
                                        <div class="col-md-9">
                                            <input 
                                            class="form-control" 
                                            id="category-parent-id" 
                                            type="text" 
                                            name="parentId" 
                                            placeholder="Enter ParentID ..."
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-1 col-form-label" for="category-main-default-id">Main-Default ID:</label>
                                        <div class="col-md-9">
                                            <input 
                                            class="form-control" 
                                            id="category-main-default-id" 
                                            type="text" 
                                            name="mainDefault" 
                                            placeholder="Enter Main-Default id ..." 
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Insert
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('trader-categories-create')) {
    ReactDOM.render(<CreateCategoriesPage />, document.getElementById('trader-categories-create'));
}
