import React, { Component } from 'react'

import DashboardSidebar from '../list/DashboardSidebar';
import { userTraderSidebar } from '../../constants';

export default class TraderBaseComponent extends Component 
{

    constructor(props) 
    {
        super();
    }

    getSidebarComponent = () => 
    {
        return <DashboardSidebar sidebar={userTraderSidebar} />
    }

    render() {
        return (
            <div>
                
            </div>
        )
    }
}
