import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import UserBaseComponent from './../UserBaseComponent';

class IndexDashboardPage extends UserBaseComponent {
    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="row">
                            <div className="col-sm-6 col-lg-3">
                                <div className="card text-white bg-primary">
                                    <div className="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                        <div>
                                            <div className="text-value-lg">9.823</div>
                                            <div>Members online</div>
                                        </div>
                                        <div className="btn-group">
                                            <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            </button>
                                            <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                                        </div>
                                    </div>
                                    <div className="c-chart-wrapper mt-3 mx-3" style={{
                                        "height": "70px"
                                    }}>
                                        <div className="chartjs-size-monitor">
                                            <div className="chartjs-size-monitor-expand">
                                                <div className=""></div>
                                            </div>
                                            <div className="chartjs-size-monitor-shrink">
                                                <div className=""></div>
                                            </div>
                                        </div>
                                        <canvas className="chart chartjs-render-monitor" id="card-chart1" style={{
                                            display: "block",
                                            width: "164px",
                                            height: "70px"
                                        }} width="164" height="70"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="card text-white bg-info">
                                    <div className="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                        <div>
                                            <div className="text-value-lg">9.823</div>
                                            <div>Members online</div>
                                        </div>
                                        <div className="btn-group">
                                            <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            </button>
                                            <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                                        </div>
                                    </div>
                                    <div className="c-chart-wrapper mt-3 mx-3" style={{
                                        "height": "70px"
                                    }}>
                                        <div className="chartjs-size-monitor">
                                            <div className="chartjs-size-monitor-expand">
                                                <div className=""></div>
                                            </div>
                                            <div className="chartjs-size-monitor-shrink">
                                                <div className=""></div>
                                            </div>
                                        </div>
                                        <canvas className="chart chartjs-render-monitor" id="card-chart2" style={{
                                            display: "block",
                                            width: "164px",
                                            height: "70px;"
                                        }} width="164" height="70"></canvas>
                                        <div id="card-chart2-tooltip" className="c-chartjs-tooltip top" style={{
                                            opacity: 0,
                                            left: "110.5px",
                                            top: "125.882px"
                                        }}>
                                            <div className="c-tooltip-header">
                                                <div className="c-tooltip-header-item">July</div>
                                            </div>
                                            <div className="c-tooltip-body">
                                                <div className="c-tooltip-body-item"><span className="c-tooltip-body-item-color" style={{
                                                    "background-color": "rgb(51, 153, 255)"
                                                }}></span><span className="c-tooltip-body-item-label">My First dataset</span><span className="c-tooltip-body-item-value">11</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="card text-white bg-warning">
                                    <div className="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                        <div>
                                            <div className="text-value-lg">9.823</div>
                                            <div>Members online</div>
                                        </div>
                                        <div className="btn-group">
                                            <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            </button>
                                            <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="card text-white bg-danger">
                                    <div className="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                        <div>
                                            <div className="text-value-lg">9.823</div>
                                            <div>Members online</div>
                                        </div>
                                        <div className="btn-group">
                                            <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            </button>
                                            <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex justify-content-between">
                                    <div>
                                        <h4 className="card-title mb-0">Traffic</h4>
                                        <div className="small text-muted">September 2019</div>
                                    </div>
                                    <div className="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">
                                        <div className="btn-group btn-group-toggle mx-3" data-toggle="buttons">
                                            <label className="btn btn-outline-secondary">
                                                <input id="option1" type="radio" name="options" autocomplete="off" /> Day
                                            </label>
                                            <label className="btn btn-outline-secondary active">
                                                <input id="option2" type="radio" name="options" autocomplete="off" checked="" /> Month
                                            </label>
                                            <label className="btn btn-outline-secondary">
                                                <input id="option3" type="radio" name="options" autocomplete="off" /> Year
                                            </label>
                                        </div>
                                        <button className="btn btn-primary" type="button">

                                        </button>
                                    </div>
                                </div>

                            </div>
                            <div className="card-footer">
                                <div className="row text-center">
                                    <div className="col-sm-12 col-md mb-sm-2 mb-0">
                                        <div className="text-muted">Visits</div>
                                        <strong>29.703 Users (40%)</strong>
                                        <div className="progress progress-xs mt-2">
                                            <div className="progress-bar bg-success" role="progressbar" style={{
                                                width: "40%"
                                            }} aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md mb-sm-2 mb-0">
                                        <div className="text-muted">Unique</div>
                                        <strong>24.093 Users (20%)</strong>
                                        <div className="progress progress-xs mt-2">
                                            <div className="progress-bar bg-info" role="progressbar" style={{
                                                width: "20%"
                                            }} aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md mb-sm-2 mb-0">
                                        <div className="text-muted">Pageviews</div>
                                        <strong>78.706 Views (60%)</strong>
                                        <div className="progress progress-xs mt-2">
                                            <div className="progress-bar bg-warning" role="progressbar" style={{
                                                width: "60%"
                                            }} aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md mb-sm-2 mb-0">
                                        <div className="text-muted">New Users</div>
                                        <strong>22.123 Users (80%)</strong>
                                        <div className="progress progress-xs mt-2">
                                            <div className="progress-bar bg-danger" role="progressbar" style={{
                                                width: "80%"
                                            }} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md mb-sm-2 mb-0">
                                        <div className="text-muted">Bounce Rate</div>
                                        <strong>40.15%</strong>
                                        <div className="progress progress-xs mt-2">
                                            <div className="progress-bar" role="progressbar" style={{
                                                width: "40%"
                                            }} aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 col-lg-4">
                                <div className="card">
                                    <div className="card-header bg-facebook content-center">

                                    </div>
                                    <div className="card-body row text-center">
                                        <div className="col">
                                            <div className="text-value-xl">89k</div>
                                            <div className="text-uppercase text-muted small">friends</div>
                                        </div>
                                        <div className="c-vr"></div>
                                        <div className="col">
                                            <div className="text-value-xl">459</div>
                                            <div className="text-uppercase text-muted small">feeds</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-4">
                                <div className="card">
                                    <div className="card-header bg-twitter content-center">

                                    </div>
                                    <div className="card-body row text-center">
                                        <div className="col">
                                            <div className="text-value-xl">973k</div>
                                            <div className="text-uppercase text-muted small">followers</div>
                                        </div>
                                        <div className="c-vr"></div>
                                        <div className="col">
                                            <div className="text-value-xl">1.792</div>
                                            <div className="text-uppercase text-muted small">tweets</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-4">
                                <div className="card">
                                    <div className="card-header bg-linkedin content-center">

                                    </div>
                                    <div className="card-body row text-center">
                                        <div className="col">
                                            <div className="text-value-xl">500+</div>
                                            <div className="text-uppercase text-muted small">contacts</div>
                                        </div>
                                        <div className="c-vr"></div>
                                        <div className="col">
                                            <div className="text-value-xl">292</div>
                                            <div className="text-uppercase text-muted small">feeds</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-12">
                                <div className="card">
                                    <div className="card-header">Traffic &amp; Sales</div>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <div className="row">
                                                    <div className="col-6">
                                                        <div className="c-callout c-callout-info">
                                                            <small className="text-muted">New Clients</small>
                                                            <div className="text-value-lg">9,123</div>
                                                        </div>
                                                    </div>
                                                    <div className="col-6">
                                                        <div className="c-callout c-callout-danger">
                                                            <small className="text-muted">Recuring Clients</small>
                                                            <div className="text-value-lg">22,643</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr className="mt-0" />
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Monday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "34%"
                                                            }} aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "78%"
                                                            }} aria-valuenow="78" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Tuesday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "56%"
                                                            }} aria-valuenow="56" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "94%"
                                                            }} aria-valuenow="94" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Wednesday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "12%"
                                                            }} aria-valuenow="12" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "67%"
                                                            }} aria-valuenow="67" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Thursday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "43%"
                                                            }} aria-valuenow="43" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "91%"
                                                            }} aria-valuenow="91" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Friday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "22%"
                                                            }} aria-valuenow="22" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "73%"
                                                            }} aria-valuenow="73" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Saturday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "53%"
                                                            }} aria-valuenow="53" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "82%"
                                                            }} aria-valuenow="82" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-4">
                                                    <div class="progress-group-prepend"><span class="progress-group-text">Sunday</span></div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-info" role="progressbar" style={{
                                                                width: "9%"
                                                            }} aria-valuenow="9" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-danger" role="progressbar" style={{
                                                                width: "69%"
                                                            }} aria-valuenow="69" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-6">
                                                <div className="row">
                                                    <div className="col-6">
                                                        <div className="c-callout c-callout-warning">
                                                            <small className="text-muted">Pageviews</small>
                                                            <div className="text-value-lg">78,623</div>
                                                        </div>
                                                    </div>
                                                    <div className="col-6">
                                                        <div className="c-callout c-callout-success">
                                                            <small className="text-muted">Organic</small>
                                                            <div className="text-value-lg">49,123</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr className="mt-0" />
                                                <div class="progress-group">
                                                    <div class="progress-group-header">
                                                        <div>Male</div>
                                                        <div class="mfs-auto font-weight-bold">43%</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-warning" role="progressbar" style={{
                                                                width: "43%"
                                                            }} aria-valuenow="43" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group mb-5">
                                                    <div class="progress-group-header">
                                                        <div>Female</div>
                                                        <div class="mfs-auto font-weight-bold">37%</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-warning" role="progressbar" style={{
                                                                width: "43%"
                                                            }} aria-valuenow="43" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group">
                                                    <div class="progress-group-header align-items-end">
                                                        <div>Organic Search</div>
                                                        <div class="mfs-auto font-weight-bold mfe-2">191.235</div>
                                                        <div class="text-muted small">(56%)</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-success" role="progressbar" style={{
                                                                width: "56%"
                                                            }} aria-valuenow="56" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group">
                                                    <div class="progress-group-header align-items-end">
                                                        <div>Facebook</div>
                                                        <div class="mfs-auto font-weight-bold mfe-2">51.223</div>
                                                        <div class="text-muted small">(15%)</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-success" role="progressbar" style={{
                                                                width: "15%"
                                                            }} aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group">
                                                    <div class="progress-group-header align-items-end">
                                                        <div>Twitter</div>
                                                        <div class="mfs-auto font-weight-bold mfe-2">37.564</div>
                                                        <div class="text-muted small">(11%)</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-success" role="progressbar" style={{
                                                                width: "11%"
                                                            }} aria-valuenow="11" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="progress-group">
                                                    <div class="progress-group-header align-items-end">
                                                        <div>LinkedIn</div>
                                                        <div class="mfs-auto font-weight-bold mfe-2">27.319</div>
                                                        <div class="text-muted small">(8%)</div>
                                                    </div>
                                                    <div class="progress-group-bars">
                                                        <div class="progress progress-xs">
                                                            <div class="progress-bar bg-success" role="progressbar" style={{
                                                                width: "8%"
                                                            }} aria-valuenow="8" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </CoreUiLayout>
        );
    }
}

if (document.getElementById('user-index')) {
    ReactDOM.render(<IndexDashboardPage />, document.getElementById('user-index'));
}
