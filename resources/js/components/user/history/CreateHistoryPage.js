import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import UserBaseComponent from './../UserBaseComponent';

export default class CreateHistoryPage extends UserBaseComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

            </CoreUiLayout>
        )
    }
}
if (document.getElementById('user-history-create')) {
    ReactDOM.render(<CreateHistoryPage />, document.getElementById('user-history-create'));
}
