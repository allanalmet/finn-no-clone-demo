import { Settings } from '@mui/icons-material';
import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import UserBaseComponent from './../UserBaseComponent';

export default class IndexSettingsPage extends UserBaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            settings: [
                {
                    id: 1,
                    name: "My account",
                    slug: "my-account"
                },
                {
                    id: 2,
                    name: "My ads",
                    slug: "my-ads"
                },
                {
                    id: 3,
                    name: "Favorites",
                    slug: "favorites"
                },
                {
                    id: 4,
                    name: "Saved searches",
                    slug: "saved-searches"
                },
                {
                    id: 5,
                    name: "Privacy",
                    slug: "privacy"
                },
                {
                    id: 6,
                    name: "Customize notifications",
                    slug: "notifications"
                },
            ],
        }
    }

    buildSettings = () => 
    {
        var settings = this.state.settings.map((item, index) => {
            const hrefUrl = "/user/settings/edit/" + item.slug;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <a href={hrefUrl} class="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div className="card">
                <div className="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i> 
                        Settings
                    </div>
                </div>
                <div className="card-body">

                    <table className="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            {settings}
                        </tbody>
                    </table>
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }


    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {this.state.settings != null ? this.buildSettings() : null}
                    </div>    
                </div>
                
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('user-settings-index')) {
    ReactDOM.render(<IndexSettingsPage />, document.getElementById('user-settings-index'));
}
