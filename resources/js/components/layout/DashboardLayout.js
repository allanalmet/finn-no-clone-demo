import React, { Component } from 'react';

import "./DashboardLayout.css";

class DashboardLayout extends Component {
    render() {
        return (
            <div className="dashboard-content">
                <div className="dashboard-content-left">
                    <ul>
                        <li>
                            <a href={"/admin/categories/index"}>Categories</a>
                        </li>
                        <li>
                            <a href={"/admin/products/index"}>Products</a>
                        </li>
                        <li>
                            <a href={"/admin/settings/index"}>Settings</a>
                        </li>
                    </ul>
                </div>
                <div className="dashboard-content-right">
                    {this.props.children}
                </div>                
            </div>
        );
    }
}

export default DashboardLayout;