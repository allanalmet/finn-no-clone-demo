import React, { Component } from 'react'

import "./CoreUiLayout.css";

export default class CoreUiLayout extends Component {
    render() {
        return (
            <div className="c-app">


                <div className="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
                    <div className="c-sidebar-brand d-lg-down-none">

                    </div>
                    {this.props.sidebar}
                    <button className="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" ></button>
                </div>


                <div className="c-wrapper c-fixed-components">
                    <header className="c-header c-header-light c-header-fixed c-header-with-subheader">
                        <button className="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar">

                        </button>
                        <a className="c-header-brand d-lg-none" href="#">

                        </a>
                        <button className="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" responsive="true">

                        </button>
                        <ul className="c-header-nav d-md-down-none">
                            <li className="c-header-nav-item px-3"><a className="c-header-nav-link" href="#">Dashboard</a></li>
                            <li className="c-header-nav-item px-3"><a className="c-header-nav-link" href="#">Users</a></li>
                            <li className="c-header-nav-item px-3"><a className="c-header-nav-link" href="#">Settings</a></li>
                        </ul>
                        <ul className="c-header-nav ml-auto mr-4">
                            <li className="c-header-nav-item d-md-down-none mx-2">
                                <a className="c-header-nav-link" href="#">

                                </a>
                            </li>
                            <li className="c-header-nav-item d-md-down-none mx-2">
                                <a className="c-header-nav-link" href="#">

                                </a>
                            </li>
                            <li className="c-header-nav-item d-md-down-none mx-2">
                                <a className="c-header-nav-link" href="#">

                                </a>
                            </li>
                            <li className="c-header-nav-item dropdown">
                                <a className="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <div className="c-avatar"><img className="c-avatar-img" src="https://coreui.io/demo/free/3.4.0/assets/img/avatars/6.jpg" alt="user@email.com"/></div>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right pt-0">
                                    <div className="dropdown-header bg-light py-2"><strong>Account</strong></div>
                                    <a className="dropdown-item" href="#">

                                        Updates<span className="badge badge-info ml-auto">42</span>
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Messages<span className="badge badge-success ml-auto">42</span>
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Tasks<span className="badge badge-danger ml-auto">42</span>
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Comments<span className="badge badge-warning ml-auto">42</span>
                                    </a>
                                    <div className="dropdown-header bg-light py-2"><strong>Settings</strong></div>
                                    <a className="dropdown-item" href="#">

                                        Profile
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Settings
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Payments<span className="badge badge-secondary ml-auto">42</span>
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Projects<span className="badge badge-primary ml-auto">42</span>
                                    </a>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="#">

                                        Lock Account
                                    </a>
                                    <a className="dropdown-item" href="#">

                                        Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <div className="c-subheader px-3">
                            <ol className="breadcrumb border-0 m-0">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item"><a href="#">Admin</a></li>
                                <li className="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </header>
                    <div className="c-body">
                        <main className="c-main">
                            {this.props.children}
                        </main>
                        <footer className="c-footer">
                            <div><a href="https://coreui.io">CoreUI</a> © 2020 creativeLabs.</div>
                            <div className="ml-auto">Powered by&nbsp;<a href="https://coreui.io/">CoreUI</a></div>
                        </footer>
                    </div>
                </div>



                <script src="https://coreui.io/demo/free/3.4.0/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
                <script src="https://coreui.io/demo/free/3.4.0/vendors/@coreui/icons/js/svgxuse.min.js"></script>
                <script src="https://coreui.io/demo/free/3.4.0/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
                <script src="https://coreui.io/demo/free/3.4.0/vendors/@coreui/utils/js/coreui-utils.js"></script>
                <script src="https://coreui.io/demo/free/3.4.0/js/main.js"></script>

            </div>
        )
    }
}
