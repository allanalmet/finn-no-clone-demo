import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';


export default class CreateTaxonomiesPage extends AdminBaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            title: 0,
        }
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("title", this.state.title);


        axios({
            method: 'post',
            url: "/api/admin/taxonomies/index",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState(prevState => ({
                name: null,
                title: 0,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }



    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Create Taxonomy</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="taxonomy-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="taxonomy-name"
                                                type="text" 
                                                name="name" 
                                                placeholder="Enter Taxonomy ..."
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="taxonomy-title">Is Title:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="taxonomy-title"
                                                type="text" 
                                                name="title" 
                                                placeholder="Enter title ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Insert
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-taxonomy-create')) {
    ReactDOM.render(<CreateTaxonomiesPage />, document.getElementById('admin-taxonomy-create'));
}
