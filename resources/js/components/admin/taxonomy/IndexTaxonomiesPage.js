import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

import "./IndexTaxonomiesPage.css";

export default class IndexTaxonomiesPage extends AdminBaseComponent {


    constructor(props) {
        super(props);
        this.state = {
            taxonomies: null
        };
    }

    openCreatePage() {
        window.location.href = "/admin/taxonomies/create";
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/taxonomies/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.taxonomies);
            $this.setState(prevState => ({
                taxonomies: response.data.taxonomies,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    buildTaxonomies = () => 
    {
        var taxonomies = this.state.taxonomies.map((item, index) => {
            const hrefUrl = "/admin/taxonomies/edit/" + item.id;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.main_default}</td>
                    <td>
                        <a href={hrefUrl} className="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div className="card">
                <div className="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i> 
                        Taxonomies
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary admin-panel-add-button" onClick={ev => this.openCreatePage()}>
                            Lisa
                        </button>
                    </div>
                </div>
                <div className="card-body">

                    <table className="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Default</th>
                                <th></th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            {taxonomies}
                        </tbody>
                    </table>
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {this.state.taxonomies != null ? this.buildTaxonomies() : null}
                    </div>    
                </div>
                
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-taxonomies-index')) {
    ReactDOM.render(<IndexTaxonomiesPage />, document.getElementById('admin-taxonomies-index'));
}
