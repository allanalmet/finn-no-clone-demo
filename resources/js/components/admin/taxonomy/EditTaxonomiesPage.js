import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

export default class EditTaxonomiesPage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const taxonomy_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            title: 0,
            taxonomy_id: taxonomy_id,

        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        console.log(123)
        axios({
            method: 'get',
            url: "/api/admin/taxonomies/edit/" + this.state.taxonomy_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.taxonomy.name,
                title: response.data.taxonomy.title,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("title", this.state.title);

        axios({
            method: 'post',
            url: "/api/admin/taxonomies/update/" + this.state.taxonomy_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: "",
                title: 0,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Taxonomy</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="taxonomy-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="taxonomy-name"
                                                type="text" 
                                                name="name"
                                                value={this.state.name} 
                                                placeholder="Enter Taxonomy ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="taxonomy-title">Is Title:</label>
                                        <div className="col-md-9">
                                            <input
                                                className="form-control"
                                                id="taxonomy-title"
                                                type="text"
                                                name="title"
                                                value={this.state.title}
                                                placeholder="Enter title ..."
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Insert
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-taxonomy-edit')) {
    ReactDOM.render(<EditTaxonomiesPage />, document.getElementById('admin-taxonomy-edit'));
}
