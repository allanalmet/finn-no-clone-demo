import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

export default class ShowProductsPage extends AdminBaseComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-products-show')) {
    ReactDOM.render(<ShowProductsPage />, document.getElementById('admin-products-show'));
}
