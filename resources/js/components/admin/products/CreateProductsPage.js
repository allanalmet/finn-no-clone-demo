import React from 'react'
import Modal from 'react-modal';
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

import "./CreateProductsPage.css";
import GalleryImageUploader from '../../ui/gallery/GalleryImageUploader';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

let subtitle;



export default class CreateProductsPage extends AdminBaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            location: "",
            price: 0,
            model_year: "",
            power: 0,
            vin: "",
            categories: null,
            isOpen: false,
            category_id: 0,
            taxonomies: null,
            image: null,
            car_colors: null,
            car_drives: null,
            car_transmissions: null,
            sale_types: null,
            selected_taxonomies: [],
            selected_car_color: "",
            selected_car_drives: "",
            selected_car_transmissions: "",
            selected_sale_types: "",
        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.onImageChange = this.onImageChange.bind(this);
        this.selectedTaxonomy = this.selectedTaxonomy.bind(this);
        this.imageSelected = this.imageSelected.bind(this);

    }

    componentDidMount = () => {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/categories/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.categories);
            $this.setState(prevState => ({
                categories: response.data.categories,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        axios({
            method: 'get',
            url: "/api/admin/car/color/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.car_colors);
            $this.setState(prevState => ({
                car_colors: response.data.car_colors,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        axios({
            method: 'get',
            url: "/api/admin/car/drive/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.car_drives);
            $this.setState(prevState => ({
                car_drives: response.data.car_drives,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        axios({
            method: 'get',
            url: "/api/admin/car/transmission/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.car_transmissions);
            $this.setState(prevState => ({
                car_transmissions: response.data.car_transmissions,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        axios({
            method: 'get',
            url: "/api/admin/sale/type/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.sale_types);
            $this.setState(prevState => ({
                sale_types: response.data.sale_types,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    onImageChange = event => {
        if (event.target.files && event.target.files[0]) {
            this.setState(prevState => ({
                image: event.target.files[0]
            }));
        }
    };

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    postData = (ev) => {
        ev.preventDefault();
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("description", this.state.description);
        formData.append("location", this.state.location);
        formData.append("price", this.state.price);
        formData.append("car_color", this.state.selected_car_color);
        formData.append("car_drive", this.state.selected_car_drives);
        formData.append("car_transmission", this.state.selected_car_transmissions);
        formData.append("sale_type", this.state.selected_sale_types);
        formData.append("taxonomies", JSON.stringify(this.state.selected_taxonomies));
        formData.append("images", JSON.stringify(this.state.image));


        axios({
            method: 'post',
            url: "/api/admin/products/index",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState(prevState => ({
                name: null,
                description: null,
                location: null,
                price: 0,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    openModal = (ev) => {
        ev.preventDefault();
        this.setState(prevState => ({
            isOpen: true,
        }));
    }

    afterOpenModal = () => {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#f00';
    }

    closeModal = () => {
        this.setState(prevState => ({
            isOpen: false,
        }));
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/taxonomies/parent/" + this.state.category_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> taxonomies -> ", response.data.taxonomies);
            $this.setState(prevState => ({
                taxonomies: response.data.taxonomies,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    selectedCategory = (id) => {
        this.setState(prevState => ({
            category_id: prevState.category_id == id ? 0 : id,
        }));
    }

    selectedTaxonomy = (id) => {
        let taxonomy = this.state.selected_taxonomies;

        if (taxonomy.includes(id)) 
        {
            taxonomy.splice(taxonomy.indexOf(id), 1);
        } 
        else 
        {
            taxonomy.push(id);
        }

        this.setState(prevState => ({
            selected_taxonomies: taxonomy,
        }));
    }

    buildCarColors = () => {
        const carColor = this.state.car_colors.map((item, index) => {
            return (
                <option key={index+1} value={item.id}>{item.name}</option>
            );
        });

        return (
            <div className="form-group selector-content">
                <label className="col-form-label" htmlFor="product-price">Car Color:</label>
                <div className="selector-content-select">
                    <select name="selected_car_color" onChange={this.changeValue}>
                        <option key={0} value={0}></option>
                        {carColor}
                    </select>                    
                </div>
            </div>
        );
    }

    buildCarDrives = () => {
        const carDrives = this.state.car_drives.map((item, index) => {
            return (
                <option key={index+1} value={item.id}>{item.name}</option>
            );
        });

        return (
            <div className="form-group selector-content">
                <label className="col-form-label" htmlFor="product-price">Car Drive:</label>
                <div className="selector-content-select">
                    <select name="selected_car_drives" onChange={this.changeValue}>
                        <option key={0} value={0}></option>
                        {carDrives}
                    </select>
                </div>
            </div>
        );
    }

    buildCarTransmissions = () => {
        const carTransmissions = this.state.car_transmissions.map((item, index) => {
            return (
                <option key={index+1} value={item.id}>{item.name}</option>
            );
        });

        return (
            <div className="form-group selector-content">
                <label className="col-form-label" htmlFor="product-price">Car Transmission:</label>
                <div className="selector-content-select">
                    <select name="selected_car_transmissions" onChange={this.changeValue}>
                        <option key={0} value={0}></option>
                        {carTransmissions}
                    </select>
                </div>
            </div>
        );
    }

    buildSaleTypes = () => {
        const saleTypes = this.state.sale_types.map((item, index) => {
            return (
                <option key={index+1} value={item.id}>{item.name}</option>
            );
        });

        return (
            <div className="form-group selector-content">
                <label className="col-form-label" htmlFor="product-price">Sale Type:</label>
                <div className="selector-content-select">
                    <select name="selected_sale_types" onChange={this.changeValue}>
                        <option key={0} value={0}></option>    
                        {saleTypes}
                    </select>
                </div>
            </div>
        );
    }

    imageSelected = (data) => {
        console.log("Upload files 12", data);
        this.setState(prevState => ({
            image: data
        }));
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <form className="form-horizontal" action="" method="post">
                            <div className="card">
                                <div className="card-header"><strong>Create Product</strong> </div>
                                <div className="card-body">                                   
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-price">Category:</label>
                                        <div className="col-md-9">
                                            <button onClick={this.openModal} className="btn btn-success">Vali</button>
                                            <Modal
                                                isOpen={this.state.isOpen}
                                                onAfterOpen={this.afterOpenModal}
                                                onRequestClose={this.closeModal}
                                                style={customStyles}
                                                contentLabel="Example Modal"
                                            >
                                                <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Category</h2>
                                                <button onClick={this.closeModal}>Close</button>
                                                <div>
                                                    {this.state.categories != null ? this.state.categories.map((item, index) => {
                                                        return (

                                                            <li value={item.id} key={index}>
                                                                <input type="checkbox" onChange={ev => this.selectedCategory(item.id)} /> {item.name}
                                                            </li>
                                                        )
                                                    }) : null}
                                                </div>

                                            </Modal>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-name" 
                                                value={this.state.name} 
                                                name="name" 
                                                type="text" 
                                                placeholder="Enter Name.."
                                                onChange={ev => this.changeValue(ev)} 
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-description">Description:</label>
                                        <div className="col-md-9">
                                            <textarea 
                                                className="form-control" 
                                                name="description" 
                                                id="product-description"
                                                value={this.state.description}
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-location">Location:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-location" 
                                                name="location" 
                                                value={this.state.location} 
                                                type="text" 
                                                placeholder="Enter Location.."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-price">Price:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-price" 
                                                name="price" 
                                                value={this.state.price} 
                                                type="text" 
                                                placeholder="Enter Price.."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div> 
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-model-year">Model Year:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-model-year" 
                                                name="model_year" 
                                                value={this.state.model_year} 
                                                type="text" 
                                                placeholder="Enter Model Year.."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div> 
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-power">Power:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-power" 
                                                name="power" 
                                                value={this.state.power} 
                                                type="text" 
                                                placeholder="Enter Power(KW).."
                                                onChange={ev => this.changeValue(ev)}
                                            />
                                        </div>
                                    </div>                                     
                                    <div className="products-info-horizontal">
                                        <div className="products-info-horizontal-left">
                                        {
                                            this.state.car_colors == null ? null : this.buildCarColors()                                            
                                        }  
                                        </div>
                                        <div className="products-info-horizontal-right">
                                        {
                                            this.state.car_drives == null ? null : this.buildCarDrives()
                                        }
                                        </div>
                                    </div>
                                    <div className="products-info-horizontal">

                                        <div className="products-info-horizontal-left">
                                        {
                                            this.state.car_transmissions == null ? null : this.buildCarTransmissions()                                        
                                        }  
                                        </div>
                                        <div className="products-info-horizontal-right">
                                        {
                                            this.state.sale_types == null ? null : this.buildSaleTypes()
                                        }
                                        </div>
  
                                    </div> 
          
                                    {
                                        this.state.category_id == 2 ? 
                                            <div className="form-group row">
                                                <label className="col-md-1 col-form-label" htmlFor="product-price">VIN:</label>
                                                <div className="col-md-9">
                                                    <input
                                                        className="form-control"
                                                        id="product-price"
                                                        name="vin"
                                                        value={this.state.vin}
                                                        type="text"
                                                        placeholder="Enter vin code.."
                                                        onChange={ev => this.changeValue(ev)}
                                                    />
                                                </div>
                                            </div>
                                        : null    
                                    }
                                </div>       
                            </div>
                            <div className="card">
                                <div className="card-header"><strong>Images</strong> </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-10">
                                            <GalleryImageUploader imageSelected={this.imageSelected} />
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header"><strong>Taxonomy</strong> </div>
                                <div className="card-body">
                                    <div className="form-group row">
                                        <div className="col-md-10">
                                            {
                                                this.state.taxonomies != null ? this.state.taxonomies.map((item, index) => {
                                                    let element = <li key={index}>
                                                        <input onChange={ev => this.selectedTaxonomy(item.id)} type="checkbox" /> {item.name}
                                                    </li>;
                                                    if (item.name.includes("h3")) {
                                                        element = <li dangerouslySetInnerHTML={{ __html: item.name }} key={index} />;
                                                    }

                                                    return element
                                                }) : null
                                            }
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div className="card">
                                <div className="card-body">
                                    <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit">
                                        Insert
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>                    
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-products-create')) {
    Modal.setAppElement('#admin-products-create');
    ReactDOM.render(<CreateProductsPage />, document.getElementById('admin-products-create'));
}