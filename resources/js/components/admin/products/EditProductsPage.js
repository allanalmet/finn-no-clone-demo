import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

export default class EditProductsPage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const product_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            description: "",
            location: "",
            price: 0,
            product_id: product_id,
        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        
        axios({
            method: 'get',
            url: "/api/admin/products/edit/" + this.state.product_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.products.name,
                description: response.data.products.description,
                location: response.data.products.location,
                price: response.data.products.price,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("description", this.state.description);
        formData.append("location", this.state.location);
        formData.append("price", this.state.price);

        axios({
            method: 'post',
            url: "/api/admin/products/update/" + this.state.product_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: null,
                description: null,
                location: null,
                price: 0,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Product</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-name" 
                                                value={this.state.name} 
                                                name="name" 
                                                type="text" 
                                                placeholder="Enter Name.."
                                                onChange={this.changeValue} 
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-description">Description:</label>
                                        <div className="col-md-9">
                                            <textarea 
                                                className="form-control" 
                                                name="description" 
                                                id="product-description"
                                                value={this.state.description}
                                                onChange={this.changeValue}
                                            >
                                            </textarea>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-location">Location:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-location" 
                                                name="location" 
                                                value={this.state.location} 
                                                type="text" 
                                                placeholder="Enter Location.."
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="product-price">Price:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="product-price" 
                                                name="price" 
                                                value={this.state.price} 
                                                type="text" 
                                                placeholder="Enter Price.."
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={this.postData} type="submit"> Insert</button>
                            </div>
                        </div>
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-products-edit')) {
    ReactDOM.render(<EditProductsPage />, document.getElementById('admin-products-edit'));
}
