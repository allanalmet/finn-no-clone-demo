import React, { Component } from 'react'

import DashboardSidebar from '../list/DashboardSidebar';
import { adminSidebar } from '../../constants';

export default class AdminBaseComponent extends Component
{

    constructor(props) 
    {
        super();
    }

    getSidebarComponent = () => 
    {
        return <DashboardSidebar sidebar={adminSidebar} />
    }

    render() {
        return (
            <div>
                
            </div>
        )
    }
}
