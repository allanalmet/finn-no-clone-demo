import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

import "./IndexTransmissionPage.css";

export default class IndexTransmissionPage extends AdminBaseComponent {


    constructor(props) {
        super(props);
        this.state = {
            car_transmissions: null
        };
    }

    openCreatePage() {
        window.location.href = "/admin/car/transmission/create";
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/car/transmission/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.car_transmissions);
            $this.setState(prevState => ({
                car_transmissions: response.data.car_transmissions,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    buildCarTransmissions = () =>
    {
        var carTransmissions = this.state.car_transmissions.map((item, index) => {
            const hrefUrl = "/admin/car/transmission/edit/" + item.id;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <a href={hrefUrl} className="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div className="card">
                <div className="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i> 
                        Car Transmissions
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary admin-panel-add-button" onClick={ev => this.openCreatePage()}>
                            Lisa
                        </button>
                    </div>
                </div>
                <div className="card-body">

                    <table className="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            {carTransmissions}
                        </tbody>
                    </table>
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {this.state.car_transmissions != null ? this.buildCarTransmissions() : null}
                    </div>    
                </div>
                
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-transmission-index')) {
    ReactDOM.render(<IndexTransmissionPage />, document.getElementById('admin-car-transmission-index'));
}
