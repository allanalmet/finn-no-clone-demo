import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

export default class EditTransmissionPage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const car_transmission_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            car_transmission_id: car_transmission_id,

        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        
        axios({
            method: 'get',
            url: "/api/admin/car/transmission/edit/" + this.state.car_transmission_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.car_transmission.name,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);

        axios({
            method: 'post',
            url: "/api/admin/car/transmission/update/" + this.state.car_transmission_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: "",
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Car Transmission</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="car-transmission-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="car-transmission-name"
                                                type="text" 
                                                name="name"
                                                value={this.state.name} 
                                                placeholder="Enter Car Transmission ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Save
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-transmission-edit')) {
    ReactDOM.render(<EditTransmissionPage />, document.getElementById('admin-car-transmission-edit'));
}
