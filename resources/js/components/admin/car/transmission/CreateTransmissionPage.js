import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';


export default class CreateTransmissionPage extends AdminBaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
        }
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);


        axios({
            method: 'post',
            url: "/api/admin/car/transmission/index",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState(prevState => ({
                name: null,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }



    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Create Car Transmission</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="car-drive-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="car-drive-name"
                                                type="text" 
                                                name="name" 
                                                placeholder="Enter Car Drive ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Insert
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-transmission-create')) {
    ReactDOM.render(<CreateTransmissionPage />, document.getElementById('admin-car-transmission-create'));
}
