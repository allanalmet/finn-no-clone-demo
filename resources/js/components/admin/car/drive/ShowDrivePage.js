import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

export default class ShowDrivePage extends AdminBaseComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-drive-show')) {
    ReactDOM.render(<ShowDrivePage />, document.getElementById('admin-car-drive-show'));
}
