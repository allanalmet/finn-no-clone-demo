import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

export default class EditDrivePage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const car_edit_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            car_edit_id: car_edit_id,

        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        
        axios({
            method: 'get',
            url: "/api/admin/car/drive/edit/" + this.state.car_edit_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.car_drive.name,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);

        axios({
            method: 'post',
            url: "/api/admin/car/drive/update/" + this.state.car_edit_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: "",
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Car Drive</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="car-drive-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="car-drive-name"
                                                type="text" 
                                                name="name"
                                                value={this.state.name} 
                                                placeholder="Enter Car Drive ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Save
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-drive-edit')) {
    ReactDOM.render(<EditDrivePage />, document.getElementById('admin-car-drive-edit'));
}
