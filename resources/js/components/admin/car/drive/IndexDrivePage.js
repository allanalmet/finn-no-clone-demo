import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

import "./IndexDrivePage.css";

export default class IndexDrivePage extends AdminBaseComponent {


    constructor(props) {
        super(props);
        this.state = {
            car_drives: null
        };
    }

    openCreatePage() {
        window.location.href = "/admin/car/drive/create";
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/car/drive/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.car_drives);
            $this.setState(prevState => ({
                car_drives: response.data.car_drives,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    buildCarDrives = () =>
    {
        var carDrives = this.state.car_drives.map((item, index) => {
            const hrefUrl = "/admin/car/drive/edit/" + item.id;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <a href={hrefUrl} className="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div className="card">
                <div className="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i> 
                        Car Drives
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary admin-panel-add-button" onClick={ev => this.openCreatePage()}>
                            Lisa
                        </button>
                    </div>
                </div>
                <div className="card-body">

                    <table className="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            {carDrives}
                        </tbody>
                    </table>
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {this.state.car_drives != null ? this.buildCarDrives() : null}
                    </div>    
                </div>
                
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-car-drive-index')) {
    ReactDOM.render(<IndexDrivePage />, document.getElementById('admin-car-drive-index'));
}
