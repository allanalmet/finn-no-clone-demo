import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

import "./IndexCategoriesPage.css";

export default class IndexCategoriesPage extends AdminBaseComponent {


    constructor(props) {
        super(props);
        this.state = {
            categories: null
        };
    }

    openCreatePage() {
        window.location.href = "/admin/categories/create";
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/admin/categories/index",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.categories);
            $this.setState(prevState => ({
                categories: response.data.categories,
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    buildCategories = () => 
    {
        var categories = this.state.categories.map((item, index) => {
            const hrefUrl = "/admin/categories/edit/" + item.id;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.main_default}</td>
                    <td>
                        <a href={hrefUrl} className="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            );
        });
        return (
            <div className="card">
                <div className="card-header">
                    <div>
                        <i className="fa fa-align-justify"></i> 
                        Categories
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary admin-panel-add-button" onClick={ev => this.openCreatePage()}>
                            Lisa
                        </button>
                    </div>
                </div>
                <div className="card-body">

                    <table className="table table-responsive-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Default</th>
                                <th></th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            {categories}
                        </tbody>
                    </table>
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                        <li className="page-item active"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>

                <div className="container-fluid">
                    <div className="fade-in">
                        {this.state.categories != null ? this.buildCategories() : null}
                    </div>    
                </div>
                
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-categories-index')) {
    ReactDOM.render(<IndexCategoriesPage />, document.getElementById('admin-categories-index'));
}
