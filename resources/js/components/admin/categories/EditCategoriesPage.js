import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../layout/CoreUiLayout';
import AdminBaseComponent from './../AdminBaseComponent';

export default class EditCategoriesPage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const category_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            parentId: "",
            mainDefault: "",
            category_id: category_id,

        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        
        axios({
            method: 'get',
            url: "/api/admin/categories/edit/" + this.state.category_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.category.name,
                parentId: response.data.category.parentId,
                mainDefault: response.data.category.mainDefault,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("parentId", this.state.parentId);
        formData.append("mainDefault", this.state.mainDefault);

        axios({
            method: 'post',
            url: "/api/admin/categories/update/" + this.state.category_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: "",
                parentId: "",
                mainDefault: "",
                });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Category</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" for="category-name">Category Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                            className="form-control" 
                                            id="category-name" 
                                            type="text" 
                                            name="name"
                                            value={this.state.name} 
                                            placeholder="Enter Category ..." 
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" for="category-parent-id">Parent ID:</label>
                                        <div className="col-md-9">
                                            <input 
                                            className="form-control" 
                                            id="category-parent-id" 
                                            type="text" 
                                            name="parentId"
                                            value={this.state.parentId} 
                                            placeholder="Enter ParentID ..."
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" for="category-main-default-id">Main-Default ID:</label>
                                        <div className="col-md-9">
                                            <input 
                                            className="form-control" 
                                            id="category-main-default-id" 
                                            type="text" 
                                            name="mainDefault"
                                            value={this.state.mainDefault}  
                                            placeholder="Enter Main-Default id ..." 
                                            onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Insert
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-categories-edit')) {
    ReactDOM.render(<EditCategoriesPage />, document.getElementById('admin-categories-edit'));
}
