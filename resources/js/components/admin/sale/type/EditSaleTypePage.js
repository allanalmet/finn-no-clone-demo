import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CoreUiLayout from '../../../layout/CoreUiLayout';
import AdminBaseComponent from './../../AdminBaseComponent';

export default class EditSaleTypePage extends AdminBaseComponent {

    constructor(props) {
        super(props);

        const splittedId = window.location.href.split("//")[1].split("/")
        const sale_type_id = splittedId[splittedId.length - 1];

        this.state = {
            name: "",
            sale_type_id: sale_type_id,

        }

        this.postData = this.postData.bind(this);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue = (ev) => {
        this.setState({
            [ev.target.name]: ev.target.value
        });
    }

    componentDidMount(){

        const $this = this;
        
        axios({
            method: 'get',
            url: "/api/admin/sale/type/edit/" + this.state.sale_type_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: response.data.sale_type.name,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    postData = (ev) => {
        const $this = this;

        //if (this.state.name == null) return;

        const formData = new FormData();
        formData.append("name", this.state.name);

        axios({
            method: 'post',
            url: "/api/admin/sale/type/update/" + this.state.sale_type_id,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            $this.setState({
                name: "",
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render() {
        return (
            <CoreUiLayout sidebar={this.getSidebarComponent()}>
                <div className="container-fluid">
                    <div className="fade-in">
                        <div className="card">
                            <div className="card-header"><strong>Edit Sale Type</strong> </div>
                            <div className="card-body">
                                <form className="form-horizontal" action="" method="post">
                                    <div className="form-group row">
                                        <label className="col-md-1 col-form-label" htmlFor="sale-type-name">Name:</label>
                                        <div className="col-md-9">
                                            <input 
                                                className="form-control" 
                                                id="sale-type-name"
                                                type="text" 
                                                name="name"
                                                value={this.state.name} 
                                                placeholder="Enter Category ..." 
                                                onChange={this.changeValue}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-sm btn-primary" onClick={ev => this.postData(ev)} type="submit"> 
                                    Save
                                </button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </CoreUiLayout>
        )
    }
}
if (document.getElementById('admin-sale-type-edit')) {
    ReactDOM.render(<EditSaleTypePage />, document.getElementById('admin-sale-type-edit'));
}
