import React, { Component } from "react";
import ReactDOM from 'react-dom';
import '@brainhubeu/react-carousel/lib/style.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import 'react-toastify/dist/ReactToastify.css';
import './FrontMainPage.css'
import MapIcon from '@mui/icons-material/Map';
import SearchIcon from '@mui/icons-material/Search';
/**/
import FrontPopularItem from "../FrontPopularItem/FrontPopularItem";

import CategoryIconMapping from "../../../services/ItemsMap/CategoryIconMapping";

import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';


export default class FrontMainPage extends Component {

    constructor() {
        super();
        this.state = {
            showSearchbar: false,
            searchBarText: "",
            categories: null,
            categoryIconMapping: new CategoryIconMapping(),
            search_items: [],
            popular_items: [
                {
                    price: 1300,
                    name: "BMW",
                    description: "X5",
                    location: "Tallinn",
                    image: "/img/510.jpg"
                },
                {
                    price: 1200,
                    name: "Audi",
                    description: "A6",
                    location: "Tallinn",
                    image: "/img/520.jpg"
                },
                {
                    price: 1200,
                    name: "Audi",
                    description: "A6",
                    location: "Tallinn",
                    image: "/img/520.jpg"
                },
                {
                    price: 1300,
                    name: "BMW",
                    description: "X5",
                    location: "Tallinn",
                    image: "/img/510.jpg"
                },
                {
                    price: 1200,
                    name: "Audi",
                    description: "A6",
                    location: "Tallinn",
                    image: "/img/520.jpg"
                },
                {
                    price: 1200,
                    name: "Audi",
                    description: "A6",
                    location: "Tallinn",
                    image: "/img/520.jpg"
                }
            ]
        }

        this.showSearchbar = this.showSearchbar.bind(this);
        this.onChangeSearchbar = this.onChangeSearchbar.bind(this);
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/front/categories",
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("response -> ", response.data.categories);
            $this.setState({ 
                categories: response.data.categories.filter(item => {
                    return item.main_default == 1
                }),
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        
    }
    
    showSearchbar = () => {
        this.setState(prevstate => ({
            showSearchbar: !prevstate.showSearchbar
        }));
    }

    renderSearchBar = () => {

        let searchBarContent = null;
        if (this.state.search_items == null) return;
        if (this.state.searchBarText === "") 
        {
            searchBarContent = this.state.search_items.map((item, index) => {
                if (!item.name.includes(this.state.searchBarText)) {
                    console.log("searchBarText -> ", this.state.searchBarText);
                    return undefined;
                }
                return (
                    <CardActions key={index}>
                        <Button size="small">{item.name} {item.description}</Button>
                    </CardActions>
                );
            }).filter(x => x != undefined);
        }

        searchBarContent = this.state.search_items.map((item, index) => {
            console.log("item.label -> ", item.label);
            if (!item.name.toLowerCase().includes(this.state.searchBarText)) {
                console.log("searchBarText -> ", this.state.searchBarText);
                return undefined;
            }
            return (
                <CardActions key={index}>
                    <Button size="small">{item.name} {item.description}</Button>
                </CardActions>
            );
        }).filter(x => x != undefined);

        return (
            <div className="front-page-content-searchbar-left-searchbox">
                <h3>Popular</h3>
                {searchBarContent}
            </div>
        );
    }

    onChangeSearchbar = (ev) => {
        ev.persist();
        
        this.setState(prevstate => ({
            searchBarText: ev.target.value
        }));

        const $this = this;
        axios({
            method: 'get',
            url: "/api/front/search/" + ev.target.value,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("Search response -> ", response.data.data);
            $this.setState({ 
                search_items: response.data.data,
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });        
    }

    render() {
        if (this.state.categories == null) return <></>
        return (
            <MuiThemeProvider>
                <main className="front-page-view">
                    <div className="front-page-content">
                        <div className="front-page-content-searchbar">
                            <div className="front-page-content-searchbar-left">
                                {/* <p>Test</p>  */}
                                <input 
                                    type="text"  
                                    placeholder="Otsing kuulutuse järgi"
                                    onClick={this.showSearchbar} 
                                    onChange={this.onChangeSearchbar}
                                    value={this.state.searchBarText}
                                /> <SearchIcon></SearchIcon>
                            </div>
                            {
                                this.state.showSearchbar == true ? this.renderSearchBar() : null
                            }
                            
                            <div className="front-page-content-searchbar-right">
                                <div className="map-find-logo">
                                    <a style={{display: "flex", alignItems: "center", color: "rgb(71, 68, 69)"}} href="someLink" target="_blank">
                                        <MapIcon className="MapIcon" fontSize="large" /> {/* ikooni suurused(fontSize) small-medium-large */}
                                        <div className="find-map-text">Leia kaardilt</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="front-page-content-categories">
                        <div className="category-content">
                            {
                                this.state.categories.map(item => {
                                    const catUrl = "/categories/" + item.id;

                                    const icon = this.state.categoryIconMapping.getIconById(item.id);

                                    console.log("icon -> ", icon);
                                    return (
                                        <div key={item.id} className="category-list">
                                            <a href={catUrl} 
                                                className="market">{icon.icon} {item.name}</a>
                                        </div>
                                    );
                                })
                            }

                        </div>
                    </div>
                    <div className="front-page-content-pushed-front-adds-title">
                        <h2>Populaarsemad kuulutused</h2>
                    </div>

                    <div className="front-page-content-pushed-front-adds">
                        <div className="front-page-content-pushed-front-adds-list">
                            {this.state.popular_items != null ?
                                this.state.popular_items.map((item, index) => {
                                    return (
                                        <FrontPopularItem key={index} popular_item={item}/>
                                    )
                                }) : null                                
                            }
                        </div>
                    </div>
                </main>
            </MuiThemeProvider>
        );
    }
}

if (document.getElementById('main_front_page')) {
    ReactDOM.render(<FrontMainPage />, document.getElementById('main_front_page'));
}
