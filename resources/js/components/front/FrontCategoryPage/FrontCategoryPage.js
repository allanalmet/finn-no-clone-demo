import React, { Component } from "react";
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CategoryMockService from "../../../services/CategoryMockService";
import HomeWorkIcon from '@mui/icons-material/HomeWork';
import "./FrontCategoryPage.css";

import CategoryIconMapping from "../../../services/ItemsMap/CategoryIconMapping";


export default class FrontCategoryPage extends Component {

    constructor() {
        super();

        const removeHttp = window.location.href.split("//")[1];
        const splitted = removeHttp.split("/");
        const categoryId = splitted[splitted.length - 1];
        const categoryMockService = new CategoryMockService();
        
        /*
        categoryMockService.getCategoriesById(categoryId)
        categoryMockService.getSubCategoriesById(categoryId)
        */
        const categoryIconMapping = new CategoryIconMapping();        
        this.state = {
            category_id: categoryId,
            category_parent: null,
            categories: null,
            icon: categoryIconMapping.getIconById(categoryId)
        }
        console.log("test");
    }

    componentDidMount() {
        const $this = this;
        axios({
            method: 'get',
            url: "/api/front/categories/" + this.state.category_id,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log(response.data.category);
            $this.setState({ 
                categories: response.data.category,
                //category_parent: response.data.parent_category 
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    render() {
        if (this.state.categories == null) return <>Loading</>
        return (
            <MuiThemeProvider>
                <div className="category_container">
                    <div className="category_container__sub_categories">
                        <div className="category_title">
                            {this.state.icon.icon}
                            <h1>{/*this.state.category_parent.name*/}</h1>
                        </div>
                        <ul>
                            {
                                this.state.categories.map(item => {
                                    const href = "/" + item.id;
                                    return (
                                        <li className="category_container__sub_categories_items" key={item.id}>
                                            <a href={href}>{item.name}</a>
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>
                    <div>

                    </div>                    
                </div>
            </MuiThemeProvider>
        );
    }
}

if (document.getElementById('main_front_category_page')) {
    ReactDOM.render(<FrontCategoryPage />, document.getElementById('main_front_category_page'));
}
