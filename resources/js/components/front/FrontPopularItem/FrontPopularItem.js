import React, { Component } from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';

import './FrontPopularItem.css'


export default class FrontPopularItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            checkBoxIsChecked: false
        }
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(ev){
        ev.preventDefault();
        this.setState(prevState=> ({checkBoxIsChecked: !prevState.checkBoxIsChecked}));
    }

    render() {
        return(
            <Card className="item-box">
                <div className="item-box-image">
                    <CardMedia
                        component="img"
                        image={this.props.popular_item.image}
                        alt="green iguana"
                    />
                </div>
                <div className="item-box-text">
                    <div>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {this.props.popular_item.label}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                <div>{this.props.popular_item.price}</div>
                                <div>{this.props.popular_item.location}</div>
                            </Typography>
                        </CardContent>
                    </div>
                    <div className="item-box-icon">
                        <CardContent>
                            {this.state.checkBoxIsChecked 
                                ? <CheckBoxIcon onClick={this.handleClick} style={{fill: "#2F8D37"}} fontSize="large"></CheckBoxIcon>  
                                : <CheckBoxOutlineBlankIcon onClick={this.handleClick} style={{fill: ""}} fontSize="large"></CheckBoxOutlineBlankIcon>
                            }
                        </CardContent>    
                    </div>
                </div>
                <CardActions>
                    <Button size="small">Share</Button>
                </CardActions>
            </Card>
        );
    }
}