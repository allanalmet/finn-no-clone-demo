import React, { Component } from "react";
import ReactDOM from 'react-dom';
import ImageLoader from "react-loading-image";
import ProductsMockData from "../../../services/ProductsMockService.js";

import ImageGallery from 'react-image-gallery';
import './FrontProductDetailPage.css';
import { purple } from "@mui/material/colors";
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';



export default class FrontProductDetailPage extends Component {

    constructor() {
        super();

        console.log("url ", window.location.href);
        let splittedUrl = window.location.href.split("//");
        let productId = splittedUrl[1].split("/")[3];
        console.log("product ID ", productId);

        console.log("mockData ", new ProductsMockData().getProductById(productId));

        const product = new ProductsMockData().getProductById(productId);

        this.state = {
            product: product,
            productId: productId,
            images: product.images.map(item => {
                const imageUrl = `/img/${item}`;                
                return {
                    original: imageUrl,
                    thumbnail: imageUrl,
                }
            }),
            checkBoxIsChecked: false
        };
        this.handleClick = this.handleClick.bind(this);

    }
    handleClick(ev){
        ev.preventDefault();
        this.setState(prevState=> ({checkBoxIsChecked: !prevState.checkBoxIsChecked}));
    }

    componentDidMount() {
        let $this = this;
        axios({
            method: 'post',
            url: '/product/detail/' + this.state.productId,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log(response);
            if (response.data.length < 1) return;
            //$this.setState({ product: response.data });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        axios({
            method: 'get',
            url: '/api/front/taxonomy/' + this.state.productId,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("taxonomies -> ", response);
            if (response.data.length < 1) return;
            $this.setState(prevstate => ({ 
                taxonomies: response.data.taxonomies 
            }));
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    buildTaxonomies = () => {
        const taxonomies = this.state.taxonomies.map(item => {
            return <div>{item.name}</div>
        });
        return (
            <div>
                <h2>Varustus</h2>
                {taxonomies}
            </div>
        );
    }

    render() {
        if (this.state.product == null) return <div>loading...</div>

        return (
            // Galerii Styling
            <div className="product-page-layout">
                <div className="product-page-layout-left"></div>
                
                <div className="product-page-layout-center">
                    <div className="product-detail-page">
                        <div className="product-name">
                            <div className="product-name-info">
                                <h2>{this.state.product.name }, {this.state.product.description}</h2>
                            </div>
{/* TODO SALVESTA LEMMIK PEAB OLEMA CHECK-BOXIGA ÜKS KLIKITAV */}
                            <div className="product-detail-page-item-box-icon"> 
                                Salvesta lemmikuks
                            {this.state.checkBoxIsChecked 
                                ? <CheckBoxIcon onClick={this.handleClick} style={{fill: "#2F8D37"}} fontSize="large"></CheckBoxIcon>  
                                : <CheckBoxOutlineBlankIcon onClick={this.handleClick} style={{fill: ""}} fontSize="large"></CheckBoxOutlineBlankIcon>
                            }
                             </div>
                        </div>
                        <div className="product-detail-content-view">
                            {/* pane row asemel oma klass */}
                            {/* Igale DIV oma classname */}
                            {/* Style blokid oma ette tõsta css ja Galerii style */}
                            <div className="product-detail-gallery">
                                    <ImageGallery
                                        lazyLoad={true}
                                        items={this.state.images}
                                    />
                            </div>

                            <div className="product-detail-description"> 
{/* TODO Siia tuleb andmebaasist auto informatsioon: mudel, mark, käigukast jne */}
                                <div>
                                    <span>Mark: </span>{this.state.product.name} <hr />
                                </div>
                                <div>
                                    <span>Mudel: </span>{this.state.product.description} <hr />
                                </div>
                                <div>
                                    <span>Hind: </span>{this.state.product.price / 100} <hr />
                                </div>
                            </div>
                        </div>
                        <hr />


                        {/* Siin div blokk ,mis tuleb galerii alla */}
                        

            {/* SIIANI SAI TEHTUD */}
            {/* Pealkirja border jäi poolikuks */}


                        <div className="product-detail-info">
                            <div className="product-detail-info-left">
                                { this.state.taxonomies ? this.buildTaxonomies() : null }
                            </div>

                            <div className="product-detail-info-right">
                                

                            </div>

                        </div>
                    </div>

                </div>
                <div className="product-page-layout-right"></div>
            </div>
        );
    }

}

if (document.getElementById('main_front_product_detail_page')) {
    ReactDOM.render(<FrontProductDetailPage />, document.getElementById('main_front_product_detail_page'));
}


