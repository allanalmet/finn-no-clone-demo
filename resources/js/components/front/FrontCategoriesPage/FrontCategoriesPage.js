import React, { Component } from "react";
import ReactDOM from 'react-dom';
import PubSub from "pubsub-js";
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FrontProductItem from '../../ui/front/FrontProductItem';
import FrontCategoryItem from '../../../list/FrontCategoryItem';

import FrontPublishedItem from "../../../list/FrontPublishedItem";

import { productsMockData } from "../../../../services/ProductsMockService.js";
import { publishedMockData } from "../../../../services/FrontFilterPublishedMockService";
import FrontAdvertiserItem from "../../../list/FrontAdvertiserItem";
import FrontFormOfSaleItem from "../../../list/FrontFormOfSaleItem";
import { advertiserMockData } from "../../../../services/FrontFilterAdvertiserMockService";
import { formOfSaleMockData } from "../../../../services/FrontFilterFormOfSaleMockService";
import { sidebarCategoriesMockData } from "../../../../services/FrontSidebarCategoriesFilter";
import { sidebarSubCategoriesMockData } from "../../../../services/FrontSidebarSubCategoriesFilter";
import { categoryTypes } from "../../../../constants";

import "./FrontCategoriesPage.css";

export default class FrontCategoriesPage extends Component {

    constructor() {
        super();
        this.state = {
            products: {
                data: productsMockData,
            },
            categories: {
                data: sidebarCategoriesMockData
            },
            published: {
                data: publishedMockData
            },
            advertisers: {
                data: advertiserMockData
            },
            formOfSales: {
                data: formOfSaleMockData
            },
            filter: {
                category: new Set(),
                advertiser: new Set(),
                formOfSale: new Set(),
                published: new Set(),
            },
            temp_products: {
                data: productsMockData
            },
            sidebarHeaderStyle: {
                "marginTop": "32px"
            },
        }


        this.buildCategoriesList = this.buildCategoriesList.bind(this);
        this.buildProductsList = this.buildProductsList.bind(this);
        this.sidebarFilter = this.sidebarFilter.bind(this);
        console.log("FrontCategoriesPage -> ");
    }

    componentDidMount() {
        let $this = this;
        axios.get("/api/front/categories")
            .then(function (response) {
                // handle success
                console.log(response);
                if (response.data.length < 1) return;
                //$this.setState({ categories: response.data });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
        axios.get("/api/front/products")
            .then(function (response) {
                // handle success
                console.log(response);
                if (response.data.length < 1) return;
                /*
                $this.setState({
                    products: response.data,
                    temp_products: response.data
                });
                */
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });

    }

    selectedCategory = (ev, category_id, category_name) => {
        ev.preventDefault();
        console.log("ev -> ", ev);
        console.log("category_id -> ", category_id);
        console.log("category_name -> ", category_name);

        let $this = this;
        axios.post("/categories/product/" + category_id + "/" + category_name)
            .then(function (response) {
                // handle success
                console.log(response);
                if (response.data.length < 1) return;
                $this.setState({ products: response.data });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    addCartEventHandler = (ev, product, quantity) => {
        ev.preventDefault();
        console.log("Added Product -> ", product);
        let formData = new FormData();
        formData.set("product_id", product.id);
        formData.set("quantity", quantity);

        let $this = this;


        /*PubSub.subscribe("MiniCartAdd", () => {
            $this.setState({
                count: $this.state.count + 1
            });
        });
        PubSub.subscribe("MiniCartAdd", () => {
            $this.setState({
                count: $this.state.count--
            });
        });*/



        PubSub.publish("MiniCartAdd", "add");
        axios({
            method: 'post',
            url: '/order/add',
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log(response);
            if (response.data.length < 1) return;
            //$this.setState({ products: response.data });
            toast.success("Successfully added !");
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toast.warn("Error ocurred added !");
        });
    }

    sidebarFilter = (ev, data) => {
        let existingFilter = this.state.filter;

        console.log("existingFilter -> ", existingFilter);

        if (data.advertiser != null && data.type == categoryTypes.advertiser) {
            console.log("Advertiser -> selected", data);
            if (existingFilter.advertiser.has(data.advertiser)) {
                toast.success("advertiser deleted !");
                existingFilter.advertiser.delete(data.advertiser);
            } else {
                toast.success("advertiser added !");
                existingFilter.advertiser.add(data.advertiser);
            }

        }

        if (data.formOfSale != null && data.type == categoryTypes.formOfSale) {
            console.log("FormOfSale -> selected", data);
            if (existingFilter.formOfSale.has(data.formOfSale)) {
                toast.success("formOfSale deleted !");
                existingFilter.formOfSale.delete(data.formOfSale);
            } else {
                toast.success("formOfSale added !");
                existingFilter.formOfSale.add(data.formOfSale);
            }

        }

        if (data.publish != null && data.type == categoryTypes.published) {
            console.log("published -> selected", data);
            if (existingFilter.published.has(data.publish)) {
                toast.success("published deleted !");
                existingFilter.published.delete(data.publish);
            } else {
                toast.success("published added !");
                existingFilter.published.add(data.publish);
            }

        }

        if (data.category != null && data.type == categoryTypes.category) {
            console.log("Category -> selected", data);
            if (existingFilter.category.has(data.category)) {
                toast.success("category deleted !");
                existingFilter.category.delete(data.category);
            } else {
                toast.success("category added !");
                existingFilter.category.add(data.category);
            }

        }

        console.log("this.state.filter -> ", this.state.filter);
        console.log("this.state.filter.category.size -> ", this.state.filter.category.size);

        //this.setState({ filter: existingFilter});



        if (this.state.filter.category.size < 1 && this.state.filter.advertiser.size < 1 && this.state.filter.formOfSale.size < 1 && this.state.filter.published.size < 1) {
            this.setState({ products: this.state.temp_products });
            toast.success("products added !");
        }
        else {


            let selectedCategory = Array.from(this.state.filter.category);
            let selectedPublished = Array.from(this.state.filter.published);
            let selectedFormOfSale = Array.from(this.state.filter.formOfSale);
            let selectedAdertiser = Array.from(this.state.filter.advertiser);
            //let selectedCategories = Array.from(this.state.filter.category);

            //let selectedStates = Array.from(this.state.filter.state);

            //console.log("SelectedCategories -> ", selectedCategories);
            //console.log("this.state.temp_products -> ", this.state.temp_products);

            let products = this.state.temp_products.data.map(item => {
                console.log("item -> ", item);
                //let selectedState = selectedStates.filter(state => state.id == item.state_id);
                
                let selectedCategoriesItem = []
                let selectedPublishedItem = []
                let selectedFormOfSaleItem = []
                let selectedAdertiserItem = []

                if (selectedCategory.length > 0){
                    selectedCategoriesItem = selectedCategory.filter(category => {
                        if (category.id == item.categories_id){
                            return item;
                        }
                    });
                }
                if (selectedPublished.length > 0 && item.created_at == 1){
                    selectedPublishedItem.push(item);
                }
                if (selectedFormOfSale.length > 0){
                    //selectedFormOfSaleItem = selectedFormOfSale.filter(formOfSale => );
                }
                if (selectedAdertiser.length > 0){
                    
                    selectedAdertiserItem = selectedAdertiser.filter(advertiser => {
                        if (advertiser.id == 3 && item.private == 1){
                            return item;
                        }
                    });
                }

                //console.log("selectedState -> ", selectedState);
                console.log("selectedCategory -> ", selectedCategory);

                if (selectedCategoriesItem.length > 0 || selectedPublishedItem.length > 0 || selectedAdertiserItem.length > 0 || selectedFormOfSaleItem.length > 0) {
                    return item;
                }
            }).filter(item => item != undefined);

            console.log("Products -> ", products);

            this.setState({
                products: {
                    data: products
                }
            });
        }

        //
        return;
        let $this = this;
        setTimeout(() => {

            let formData = new FormData();
            if ($this.state.filter.state.size > 0) {
                console.log("Array.from -> ", JSON.stringify(Array.from($this.state.filter.state)));

                formData.append("states", JSON.stringify(Array.from($this.state.filter.state)));
            }

            if ($this.state.filter.category.size > 0) {
                console.log("Array.from -> ", JSON.stringify(Array.from($this.state.filter.category)));

                formData.append("categories", JSON.stringify(Array.from($this.state.filter.category)));
            }

            axios({
                method: 'post',
                url: '/products/filter',
                data: formData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(function (response) {
                    // handle success
                    console.log(response);

                    $this.setState({ products: response.data });

                    if (response.data.length < 1) return;
                    //$this.setState({ products: response.data });
                    //toast.success("Successfully added !");
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                    //toast.warn("Error ocurred added !");
                });

        }, 200)

    }

    buildProductsList() {

        let products = this.state.products.data.map((item, index) => {
            return (<FrontProductItem addCartEventHandler={this.addCartEventHandler} key={item.id + "-" + item.name} product={item} products={this.state.products} />);
        })

        return (<div><div className="col-md-12"><div className="row">{products}</div></div></div>);
    }

    buildCategoriesList() {
        let categories = this.state.categories.data.map((item, index) => {
            let subcats = sidebarSubCategoriesMockData.map(subCategories => {
                return item.id == subCategories.parentId
                ? <FrontCategoryItem sidebarFilter={this.sidebarFilter} category={subCategories} key={subCategories.id + "-" + subCategories.name} selectedCategory={this.selectedCategory} />
                : undefined;
            }).filter(x => x != undefined);
            return [
                <FrontCategoryItem sidebarFilter={this.sidebarFilter} category={item} key={item.id + "-" + item.name} selectedCategory={this.selectedCategory} />,
                ...subcats
            ]
        }).filter(x => x != undefined);

        console.log("message categories ", categories);

        return categories;
    }

    buildPublishedList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Published</h5>{
            this.state.published.data.map((item, index) => {
                return (<FrontPublishedItem sidebarFilterMain={this.sidebarFilter} publishMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildAdvertiserList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Advertiser</h5>{
            this.state.advertisers.data.map((item, index) => {
                return (<FrontAdvertiserItem sidebarFilterMain={this.sidebarFilter} advertiserMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildFormOfSaleList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Form Of Sale</h5>{
            this.state.formOfSales.data.map((item, index) => {
                return (<FrontFormOfSaleItem sidebarFilterMain={this.sidebarFilter} formOfSaleMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildStatesList() {
        return (<div className="col-md-8"><h5>Piirkond</h5>{
            this.state.states.data.map((item, index) => {
                return (<FrontStatesItem sidebarFilter={this.sidebarFilter} state={item} key={item.id + "-" + item.name} selectedCategory={this.selectedCategory} />);
            })
        }</div>);
    }

    buildSpecialOffers = () => {

        return (
            <Carousel
                slidesPerPage={4}
                arrows
                centered
                infinite
                autoPlay={5000}
                animationSpeed={2000}
                dots
            >
                {
                    this.state.specialOffers.map(item => {
                        return (<img style={{ "height": "150px" }} src={item.src} />);
                    })
                }
            </Carousel>
        );
    }

    render() {

        return (
            <MuiThemeProvider>
                <div className="row justify-content-center">
                    <div className="col-md-2">
                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.categories != null && this.state.products != null ? this.buildCategoriesList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.published != null && this.state.products != null ? this.buildPublishedList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.advertisers != null && this.state.products != null ? this.buildAdvertiserList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.formOfSales != null && this.state.products != null ? this.buildFormOfSaleList() : null}
                            </ul>
                        </div>

                    </div>
                    <div className="col-md-10">
                        {this.state.products != null ? this.buildProductsList() : null}
                    </div>
                    <ToastContainer />
                </div>
            </MuiThemeProvider>
        );
    }
}

if (document.getElementById('main_front_categories_page')) {
    ReactDOM.render(<FrontCategoriesPage />, document.getElementById('main_front_categories_page'));
}
