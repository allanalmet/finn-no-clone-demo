import React, { Component } from "react";
import ReactDOM from 'react-dom';
import PubSub from "pubsub-js";
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CategoryProductItem from '../../ui/front/CategoryProductItem';
import FrontCategoryItem from '../../list/FrontCategoryItem';

import FrontPublishedItem from "../../list/FrontPublishedItem";

import { productsMockData } from "../../../services/ProductsMockService.js";
import { publishedMockData } from "../../../services/FrontFilterPublishedMockService";
import FrontAdvertiserItem from "../../list/FrontAdvertiserItem";
import FrontFormOfSaleItem from "../../list/FrontFormOfSaleItem";
import { advertiserMockData } from "../../../services/FrontFilterAdvertiserMockService";
import { formOfSaleMockData } from "../../../services/FrontFilterFormOfSaleMockService";
import { sidebarCategoriesMockData } from "../../../services/FrontSidebarCategoriesFilter";
import { sidebarSubCategoriesMockData } from "../../../services/FrontSidebarSubCategoriesFilter";
import { categoryTypes } from "../../../constants";
import { ConstructionOutlined } from "@mui/icons-material";

import "./FrontCategoryItemsPage.css";

export default class FrontCategoryItemsPage extends Component {

    constructor() {
        super();

        const removeHttp = window.location.href.split("//")[1];
        const splitted = removeHttp.split("/");
        const categoryId = splitted[splitted.length - 1];

        this.state = {
            products: {
                data: [],
            },
            categories: {
                data: []
            },
            published: {
                data: publishedMockData
            },
            advertisers: {
                data: advertiserMockData
            },
            formOfSales: {
                data: formOfSaleMockData
            },
            filter: {
                category: new Set(),
                advertiser: new Set(),
                formOfSale: new Set(),
                published: new Set(),
            },
            temp_products: {
                data: []
            },
            sidebarHeaderStyle: {
                "marginTop": "32px"
            },
            category_id: categoryId,
            category_types: []
        }


        this.buildCategoriesList = this.buildCategoriesList.bind(this);
        this.buildProductsList = this.buildProductsList.bind(this);
        this.sidebarFilter = this.sidebarFilter.bind(this);
        console.log("testing");
    }

    componentDidMount() {
        let $this = this;
        console.log("URL -> ", window.location.href);
        axios.get("/api/front/categories/" + this.state.category_id)
            .then(function (response) {
                // handle success
                console.log("Categories -> cats -> ", response);
                if (response.data.length < 1) return;
                $this.setState({ 
                    categories: { 
                        data: response.data.category 
                    } 
                })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
        /*    
        axios.post("/category/" + this.state.category_id + "/products")
            .then(function (response) {
                // handle success
                console.log("Categories -> products -> ", response);
                if (response.data.products.length < 1) return;
                $this.setState({
                    products: { data: response.data.products },
                    temp_products: { data: response.data.products }
                });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
        */    
    }

    selectedCategory = (ev, category_id, category_name) => {
        ev.preventDefault();
        console.log("ev -> ", ev);
        console.log("category_id -> ", category_id);
        console.log("category_name -> ", category_name);

        let $this = this;
        axios.post("/categories/product/" + category_id + "/" + category_name)
            .then(function (response) {
                // handle success
                console.log(response);
                if (response.data.length < 1) return;
                $this.setState({ products: response.data });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    addCartEventHandler = (ev, product, quantity) => {
        ev.preventDefault();
        console.log("Added Product -> ", product);
        let formData = new FormData();
        formData.set("product_id", product.id);
        formData.set("quantity", quantity);

        let $this = this;


        /*PubSub.subscribe("MiniCartAdd", () => {
            $this.setState({
                count: $this.state.count + 1
            });
        });
        PubSub.subscribe("MiniCartAdd", () => {
            $this.setState({
                count: $this.state.count--
            });
        });*/



        PubSub.publish("MiniCartAdd", "add");
        axios({
            method: 'post',
            url: '/order/add',
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .then(function (response) {
                // handle success
                console.log(response);
                if (response.data.length < 1) return;
                //$this.setState({ products: response.data });
                toast.success("Successfully added !");
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                toast.warn("Error ocurred added !");
            });
    }

    sidebarFilter = (ev, data, toggled, fetch_products = false) => {
        let existingFilter = this.state.filter;
        let $this = this;

        console.log("data -> ", data);

        if (toggled && !fetch_products)
        {
            const catTypes = this.state.category_types.map(xx => {
                return xx.parent_id == data.category.id ? undefined : xx                
            }).filter(x => x != undefined);

            $this.setState(prevstate => ({
                category_types: catTypes
            }));

            console.log("catTypes -> ", catTypes);
            return;
        }

        console.log("existingFilter -> ", existingFilter);
        console.log("data -> ", data);
        
        const productUrl = fetch_products 
            ? "/api/front/categorytypes/" + data.id + "/products" 
            :  "/api/front/category/" + data.category.id + "/products/all"

        axios({
            method: 'get',
            url: productUrl,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("Categories -> products -> ", response);
            if (response.data.products.length < 1) return;
            $this.setState(prevState => ({
                products: { 
                    data: [
                        ...prevState.products.data,
                        ...response.data.products
                    ] 
                },
                temp_products: { data: response.data.products }
            }));

            toast.success("Successfully added !");
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toast.warn("Error ocurred added !");
        });

        axios({
            method: 'get',
            url: '/api/front/categorytypes/' + 14,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(function (response) {
            // handle success
            console.log("categorytypes -> ", response);
            if (response.data.category_types.length < 1) return;
            if ($this.state.category_types == null)
            {
                /*
                $this.setState(prevstate => ({
                    category_types: []
                }));
                */
            } 
            const hasCategory = $this.state.category_types.filter(item => item.parent_id == data.category.id);

            if (hasCategory.length > 0) return;

            console.log("hasCategory --> ", hasCategory);

            $this.setState(prevstate => ({
                category_types: [
                    ...prevstate.category_types,
                    {
                        parent_id: data.category.id,
                        data: response.data.category_types
                    }
                ]
            }));

            toast.success("Successfully added !");
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toast.warn("Error ocurred added !");
        });


        if (data.advertiser != null && data.type == categoryTypes.advertiser) {
            console.log("Advertiser -> selected", data);
            if (existingFilter.advertiser.has(data.advertiser)) {
                toast.success("advertiser deleted !");
                existingFilter.advertiser.delete(data.advertiser);
            } else {
                toast.success("advertiser added !");
                existingFilter.advertiser.add(data.advertiser);
            }

        }

        if (data.formOfSale != null && data.type == categoryTypes.formOfSale) {
            console.log("FormOfSale -> selected", data);
            if (existingFilter.formOfSale.has(data.formOfSale)) {
                toast.success("formOfSale deleted !");
                existingFilter.formOfSale.delete(data.formOfSale);
            } else {
                toast.success("formOfSale added !");
                existingFilter.formOfSale.add(data.formOfSale);
            }

        }

        if (data.publish != null && data.type == categoryTypes.published) {
            console.log("published -> selected", data);
            if (existingFilter.published.has(data.publish)) {
                toast.success("published deleted !");
                existingFilter.published.delete(data.publish);
            } else {
                toast.success("published added !");
                existingFilter.published.add(data.publish);
            }

        }

        if (data.category != null && data.type == categoryTypes.category) {
            console.log("Category -> selected", data);
            if (existingFilter.category.has(data.category)) {
                toast.success("category deleted !");
                existingFilter.category.delete(data.category);
            } else {
                toast.success("category added !");
                existingFilter.category.add(data.category);
            }

        }

        console.log("this.state.filter -> ", this.state.filter);
        console.log("this.state.filter.category.size -> ", this.state.filter.category.size);

        //this.setState({ filter: existingFilter});



        if (this.state.filter.category.size < 1 && this.state.filter.advertiser.size < 1 && this.state.filter.formOfSale.size < 1 && this.state.filter.published.size < 1) {
            this.setState({ products: this.state.temp_products });
            toast.success("products added !");
        }
        else {


            let selectedCategory = Array.from(this.state.filter.category);
            let selectedPublished = Array.from(this.state.filter.published);
            let selectedFormOfSale = Array.from(this.state.filter.formOfSale);
            let selectedAdertiser = Array.from(this.state.filter.advertiser);
            //let selectedCategories = Array.from(this.state.filter.category);

            //let selectedStates = Array.from(this.state.filter.state);

            //console.log("SelectedCategories -> ", selectedCategories);
            //console.log("this.state.temp_products -> ", this.state.temp_products);

            let products = this.state.temp_products.data.map(item => {
                console.log("////temp_products.item -> ", item);
                //let selectedState = selectedStates.filter(state => state.id == item.state_id);

                let selectedCategoriesItem = []
                let selectedPublishedItem = []
                let selectedFormOfSaleItem = []
                let selectedAdertiserItem = []

                if (selectedCategory.length > 0) {
                    selectedCategoriesItem = selectedCategory.filter(category => {
                        console.log("////category_id -> ", category.category_id);
                        console.log("////category_type_id -> ", item.category_type_id);
                        if (category.id == item.category_type_id) {
                            return item;
                        }
                    });
                    console.log("selectedCategoriesItem -> ", selectedCategoriesItem)
                }
                console.log("Date() -> created_at -> ", item.created_at)
                console.log("Date() -> Date created_at -> ", new Date(item.created_at).getDate())
                console.log("Date() -> ", new Date().getDate())
                if (selectedPublished.length > 0 && new Date(item.created_at).getDate() == new Date().getDate()) {
                    selectedPublishedItem.push(item);
                }
                if (selectedFormOfSale.length > 0) {
                    //selectedFormOfSaleItem = selectedFormOfSale.filter(formOfSale => );
                }
                if (selectedAdertiser.length > 0) {

                    selectedAdertiserItem = selectedAdertiser.filter(advertiser => {
                        if (advertiser.id == 3 && item.private == 1) {
                            return item;
                        }
                    });
                }

                //console.log("selectedState -> ", selectedState);
                console.log("selectedCategory -> ", selectedCategory);

                if (selectedCategoriesItem.length > 0 || selectedPublishedItem.length > 0 || selectedAdertiserItem.length > 0 || selectedFormOfSaleItem.length > 0) {
                    return item;
                }
            }).filter(item => item != undefined);

            console.log("Products -> ", products);

            this.setState({
                products: {
                    data: products
                }
            });
        }

        //
        return;
        setTimeout(() => {

            let formData = new FormData();
            if ($this.state.filter.state.size > 0) {
                console.log("Array.from -> ", JSON.stringify(Array.from($this.state.filter.state)));

                formData.append("states", JSON.stringify(Array.from($this.state.filter.state)));
            }

            if ($this.state.filter.category.size > 0) {
                console.log("Array.from -> ", JSON.stringify(Array.from($this.state.filter.category)));

                formData.append("categories", JSON.stringify(Array.from($this.state.filter.category)));
            }

            axios({
                method: 'post',
                url: '/products/filter',
                data: formData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(function (response) {
                    // handle success
                    console.log(response);

                    $this.setState({ products: response.data });

                    if (response.data.length < 1) return;
                    //$this.setState({ products: response.data });
                    //toast.success("Successfully added !");
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                    //toast.warn("Error ocurred added !");
                });

        }, 200)

    }

    buildProductsList() {

        let products = this.state.products.data.map((item, index) => {
            return (
                <CategoryProductItem 
                    addCartEventHandler={this.addCartEventHandler} 
                    key={item.id + "-" + item.name}
                    products={this.state.products} 
                    product={item}                      
                />
            );
        })

        return (<div><div className="col-md-12"><div className="products-contents">{products}</div></div></div>);
    }

    buildCategoriesList() {
        let categories = this.state.categories.data.map((item, index) => {

            let hasSubCategories = null;

            if (this.state.category_types != null)
            {
                hasSubCategories = this.state.category_types.filter(x => {
                    return x.parent_id == item.id
                })[0];

                console.log("hasSubCategories filter -> ", hasSubCategories);

                if (hasSubCategories != null || hasSubCategories != undefined)
                {
                    hasSubCategories = hasSubCategories.data;
                }
                
            }

            console.log("hasSubCategories -> ", hasSubCategories);

            return <FrontCategoryItem 
                sidebarFilter={this.sidebarFilter} 
                category={item} key={item.id + "-" + item.name} 
                selectedCategory={this.selectedCategory}
                category_types={hasSubCategories}
            />

            let subcats = sidebarSubCategoriesMockData.map(subCategories => {
                return item.id == subCategories.parentId
                    ? <FrontCategoryItem sidebarFilter={this.sidebarFilter} category={subCategories} key={subCategories.id + "-" + subCategories.name} selectedCategory={this.selectedCategory} />
                    : undefined;
            }).filter(x => x != undefined);
            return [
                <FrontCategoryItem sidebarFilter={this.sidebarFilter} category={item} key={item.id + "-" + item.name} selectedCategory={this.selectedCategory} />,
                ...subcats
            ]
        }).filter(x => x != undefined);

        console.log("message categories ", categories);

        return categories;
    }

    buildPublishedList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Published</h5>{
            this.state.published.data.map((item, index) => {
                return (<FrontPublishedItem sidebarFilterMain={this.sidebarFilter} publishMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildAdvertiserList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Advertiser</h5>{
            this.state.advertisers.data.map((item, index) => {
                return (<FrontAdvertiserItem sidebarFilterMain={this.sidebarFilter} advertiserMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildFormOfSaleList() {
        return (<div style={this.state.sidebarHeaderStyle} className="col-md-12"><h5>Form Of Sale</h5>{
            this.state.formOfSales.data.map((item, index) => {
                return (<FrontFormOfSaleItem sidebarFilterMain={this.sidebarFilter} formOfSaleMain={item} key={item.id + "-" + item.name} />);
            })
        }</div>);
    }

    buildStatesList() {
        return (<div className="col-md-8"><h5>Piirkond</h5>{
            this.state.states.data.map((item, index) => {
                return (<FrontStatesItem sidebarFilter={this.sidebarFilter} state={item} key={item.id + "-" + item.name} selectedCategory={this.selectedCategory} />);
            })
        }</div>);
    }

    buildSpecialOffers = () => {

        return (
            <Carousel
                slidesPerPage={4}
                arrows
                centered
                infinite
                autoPlay={5000}
                animationSpeed={2000}
                dots
            >
                {
                    this.state.specialOffers.map(item => {
                        return (<img style={{ "height": "150px" }} src={item.src} />);
                    })
                }
            </Carousel>
        );
    }

    render() {

        return (
            <MuiThemeProvider>
                <div className="page-contents">
                    <div className="page-contents-categories">
                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.categories != null && this.state.products != null ? this.buildCategoriesList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.published != null && this.state.products != null ? this.buildPublishedList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.advertisers != null && this.state.products != null ? this.buildAdvertiserList() : null}
                            </ul>
                        </div>

                        <div style={this.state.category_style} className="row">
                            <ul className="list-group">
                                {this.state.formOfSales != null && this.state.products != null ? this.buildFormOfSaleList() : null}
                            </ul>
                        </div>

                    </div>
                    <div className="page-contents-products">
                        {this.state.products != null ? this.buildProductsList() : null}
                    </div>
                    <ToastContainer />
                </div>
            </MuiThemeProvider>
        );
    }
}

if (document.getElementById('main_front_category_items_page')) {
    ReactDOM.render(<FrontCategoryItemsPage />, document.getElementById('main_front_category_items_page'));
}
