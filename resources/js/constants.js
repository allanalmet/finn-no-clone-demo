export const categoryTypes = {
    advertiser: "advertiser",
    formOfSale: "form_of_sale",
    published: "published",
    category: "category",
    subCategory: "sub_category",

};

export const adminSidebar = [
    {
        label: "Home",
        url: "/admin/index"
    },
    {
        label: "Categories",
        url: "/admin/categories/index"
    },    
    {
        label: "Products",
        url: "/admin/products/index"
    },    
    {
        label: "Settings",
        url: "/admin/settings/index"
    },
    {
        label: "Taxonomies",
        url: "/admin/taxonomies/index"
    },
    {
        label: "Car Color",
        url: "/admin/car/color/index"
    },
    {
        label: "Car Drive",
        url: "/admin/car/drive/index"
    },
    {
        label: "Car Transmission",
        url: "/admin/car/transmission/index"
    },
    {
        label: "Sale Type",
        url: "/admin/sale/type/index"
    },
];

export const userTraderSidebar = [
    {
        label: "Home",
        url: "/user/index"
    },
    {
        label: "Categories",
        url: "/trader/categories/index"
    },
    {
        label: "Products",
        url: "/trader/products/index"
    },
    {
        label: "Settings",
        url: "/user/settings/index"
    },
    {
        label: "History",
        url: "/user/history/index"
    },
];
