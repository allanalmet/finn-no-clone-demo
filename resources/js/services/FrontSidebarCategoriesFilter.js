
export const sidebarCategoriesMockData = [
    {
        id: 1,
        name: "testkategooria1",
        parentId: 1,
    },
    {
        id: 2,
        name: "testkategooria2",
        parentId: 2,
    },
    {
        id: 3,
        name: "testkategooria3",
        parentId: 3,
    },
    {
        id: 4,
        name: "testkategooria4",
        parentId: 4,
    },
    
]


export default class FrontSidebarCategoriesFilter
{
    getSidebarCategoriesById(id)
    {
        return sidebarCategoriesMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}
