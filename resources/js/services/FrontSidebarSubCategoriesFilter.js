
export const sidebarSubCategoriesMockData = [
    
    {
        id: 5,
        name: "testkategooria5",
        parentId: 1,
    },
    {
        id: 6,
        name: "testkategooria6",
        parentId: 1,
    },
    {
        id: 7,
        name: "testkategooria7",
        parentId: 1,
    },
    {
        id: 8,
        name: "testkategooria8",
        parentId: 2,
    },
]


export default class FrontSidebarSubCategoriesFilter
{
    getSidebarSubCategoriesById(id)
    {
        return sidebarSubCategoriesMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}
