
export const advertiserMockData = [
    {
        id: 1,
        name: "Showrooms",
        count: 3,
    },
    {
        id: 2,
        name: "Brand Dealers",
        count: 27,
    },
    {
        id: 3,
        name: "Private",
        count: 13337,
    },
    
];

export default class FrontFilterAdvertiserMockService
{
    getAdvertiserById(id)
    {
        return advertiserMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}
