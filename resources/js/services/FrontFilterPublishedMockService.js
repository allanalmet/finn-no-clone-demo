
export const publishedMockData = [
    {
        id: 1,
        name: "New today",
        count: 2677,
    },
];

export default class FrontFilterPublishedMockService
{
    getPublishedById(id)
    {
        return publishedMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}
