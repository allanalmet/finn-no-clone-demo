import React, { Component } from 'react';

import CommuteIcon from '@mui/icons-material/Commute';
import DevicesOtherIcon from '@mui/icons-material/DevicesOther';
import HomeWorkIcon from '@mui/icons-material/HomeWork';
import GavelIcon from '@mui/icons-material/Gavel';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';

const categoryIconMapping = [
    {
        id: 1,
        icon: <DevicesOtherIcon fontSize="large"/>
    },
    {
        id: 2,
        icon: <CommuteIcon fontSize="large"/>
    },
    {
        id: 3,
        icon: <HomeWorkIcon fontSize="large"/>
    },
    {
        id: 4,
        icon: <WorkOutlineIcon fontSize="large"/>
    },
    {
        id: 5,
        icon: <GavelIcon fontSize="large"/>
    },
];

class CategoryIconMapping extends Component {

    getIconById(id)
    {
        return categoryIconMapping.filter(item => {
            return item.id == id;
        })[0];
    }
}

export default CategoryIconMapping;