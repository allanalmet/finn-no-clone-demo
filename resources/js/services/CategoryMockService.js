
export const categoriesMockData = [
    {
        id: 1,
        name: "Kategooria1",
        created_at: 1,
        parent_id: 1,
        products_count: 12000
    },
    {
        id: 2,
        name: "Kategooria2",
        created_at: 1,
        parent_id: 1,
        products_count: 22000
    },
    {
        id: 3,
        name: "Kategooria3",
        created_at: 1,
        parent_id: 3,
        products_count: 43000
    },
    {
        id: 4,
        name: "Kategooria4",
        created_at: 1,
        parent_id: 3,
        products_count: 34000
    },
    {
        id: 5,
        name: "Kategooria5",
        created_at: 1,
        parent_id: 1,
        products_count: 46000
    },
    {
        id: 6,
        name: "Kategooria6",
        created_at: 1,
        parent_id: 1,
        products_count: 64000
    },
    {
        id: 7,
        name: "Kategooria7",
        created_at: 1,
        parent_id: 1,
        products_count: 43000
    },
    {
        id: 8,
        name: "Kategooria8",
        created_at: 1,
        parent_id: 1,
        products_count: 23000
    },
];

export default class CategoryMockService {
    getCategoriesById(id) {
        return categoriesMockData.filter(item => {
            return item.id == id;
        })[0];
    }

    getSubCategoriesById(id) {
        return categoriesMockData.map(item => {
            return item.parent_id == id ? item : undefined;
        }).filter(x => x != undefined);
    }
}