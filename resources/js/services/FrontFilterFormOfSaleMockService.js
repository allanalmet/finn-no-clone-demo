
export const formOfSaleMockData = [
    {
        id: 1,
        name: "Used Car For Sale",
        count: 82677,
    },
    {
        id: 2,
        name: "Leasing",
        count: 1347,
    },
    {
        id: 3,
        name: "New Car For Sale",
        count: 188,
    },
    {
        id: 4,
        name: "Auction",
        count: 502,
    },
];

export default class FrontFilterFormOfSaleMockService
{
    getFormOfSaleById(id)
    {
        return formOfSaleMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}
