
export const productsMockData = [
    {
        id: 1,
        name: "BMW",
        description: "316i",
        price: 300000,
        categories_id: 1,
        image: "510.jpg",
        images: [
            "510.jpg",
            "520.jpg",
            "510.jpg",
            "520.jpg",
        ],
        created_at: 1,
        private: 1
    },
    {
        id: 2,
        name: "BMW",
        description: "316i",
        price: 400000,
        categories_id: 2,
        image: "520.jpg",
        images: [
            "520.jpg",
            "520.jpg",
            "510.jpg",
            "510.jpg",

        ],
        created_at: 1,
        private: 0
    },
    {
        id: 3,
        name: "BMW",
        description: "316i",
        price: 500000,
        categories_id: 3,
        image: "510.jpg",
        images: [
            "510.jpg",
            "510.jpg",
            "520.jpg",
            "520.jpg",

        ],
        created_at: 1,
        private: 0
    },
    {
        id: 4,
        name: "BMW",
        description: "316i",
        price: 600000,
        categories_id: 4,
        image: "520.jpg",
        images: [
            "520.jpg",
            "510.jpg",
            "520.jpg",
            "510.jpg",
        ],
        created_at: 0,
        private: 1
    },
    {
        id: 5,
        name: "BMW",
        description: "316i",
        price: 500000,
        categories_id: 3,
        image: "510.jpg",
        images: [
            "510.jpg",
            "510.jpg"
        ],
        created_at: 1,
        private: 1
    },
    {
        id: 6,
        name: "BMW",
        description: "316i",
        price: 600000,
        categories_id: 4,
        image: "520.jpg",
        images: [
            "520.jpg",
            "520.jpg"
        ],
        created_at: 0,
        private: 1
    },
    {
        id: 7,
        name: "BMW",
        description: "316i",
        price: 500000,
        categories_id: 3,
        image: "510.jpg",
        images: [
            "510.jpg",
            "510.jpg"
        ],
        created_at: 0,
        private: 0
    },
    {
        id: 8,
        name: "BMW",
        description: "316i",
        price: 600000,
        categories_id: 4,
        image: "520.jpg",
        images: [
            "520.jpg",
            "520.jpg"
        ],
        created_at: 1,
        private: 0
    },
    {
        id: 9,
        name: "BMW",
        description: "316i",
        price: 500000,
        categories_id: 3,
        image: "510.jpg",
        images: [
            "510.jpg",
            "510.jpg"
        ],
        created_at: 0,
        private: 0
    },
    {
        id: 10,
        name: "BMW",
        description: "316i",
        price: 600000,
        categories_id: 4,
        image: "520.jpg",
        images: [
            "520.jpg",
            "520.jpg"
        ],
        created_at: 1,
        private: 0
    },

];

export default class ProductsMockService
{
    getProductById(id) 
    {
        return productsMockData.filter(item => {
            return item.id == id;
        })[0];
    }
}