/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');
const Axios = require('axios');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/front/FrontProductDetailPage/FrontProductDetailPage');
require('./components/front/FrontMainPage/FrontMainPage');
require('./components/front/FrontCategoryPage/FrontCategoryPage');
require('./components/front/FrontCategoryItemsPage/FrontCategoryItemsPage');

require('./components/admin/dashboard/IndexDashboardPage');

require('./components/admin/categories/CreateCategoriesPage');
require('./components/admin/categories/EditCategoriesPage');
require('./components/admin/categories/IndexCategoriesPage');
require('./components/admin/categories/ShowCategoriesPage');

require('./components/admin/products/CreateProductsPage');
require('./components/admin/products/EditProductsPage');
require('./components/admin/products/IndexProductsPage');
require('./components/admin/products/ShowProductsPage');

require('./components/admin/settings/CreateSettingsPage');
require('./components/admin/settings/EditSettingsPage');
require('./components/admin/settings/IndexSettingsPage');
require('./components/admin/settings/ShowSettingsPage');

require('./components/admin/taxonomy/IndexTaxonomiesPage');
require('./components/admin/taxonomy/CreateTaxonomiesPage');
require('./components/admin/taxonomy/EditTaxonomiesPage');

require('./components/admin/car/color/IndexColorPage');
require('./components/admin/car/color/CreateColorPage');
require('./components/admin/car/color/EditColorPage');

require('./components/admin/car/drive/IndexDrivePage');
require('./components/admin/car/drive/CreateDrivePage');
require('./components/admin/car/drive/EditDrivePage');

require('./components/admin/car/transmission/IndexTransmissionPage');
require('./components/admin/car/transmission/CreateTransmissionPage');
require('./components/admin/car/transmission/EditTransmissionPage');

require('./components/admin/sale/type/IndexSaleTypePage');
require('./components/admin/sale/type/CreateSaleTypePage');
require('./components/admin/sale/type/EditSaleTypePage');


require('./components/trader/dashboard/IndexDashboardPage');

require('./components/trader/categories/CreateCategoriesPage');
require('./components/trader/categories/EditCategoriesPage');
require('./components/trader/categories/IndexCategoriesPage');
require('./components/trader/categories/ShowCategoriesPage');

require('./components/trader/products/CreateProductsPage');
require('./components/trader/products/EditProductsPage');
require('./components/trader/products/IndexProductsPage');
require('./components/trader/products/ShowProductsPage');

require('./components/trader/dashboard/IndexDashboardPage');

require('./components/user/history/CreateHistoryPage');
require('./components/user/history/EditHistoryPage');
require('./components/user/history/IndexHistoryPage');
require('./components/user/history/ShowHistoryPage');

require('./components/user/settings/CreateSettingsPage');
require('./components/user/settings/EditSettingsPage');
require('./components/user/settings/IndexSettingsPage');
require('./components/user/settings/ShowSettingsPage');



if (document.querySelector('input[name="_token"]'))
{
    const token = document.querySelector('input[name="_token"]').value;
    Axios.defaults.headers.common['X-CSRF-TOKEN'] = token;
}
