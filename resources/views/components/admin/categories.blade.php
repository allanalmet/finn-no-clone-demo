@extends('layouts.master')

@section('content')
<div class="row">
    <div class="container">

        @foreach($categories as $c)
            <div class="row">
                ID: {{ $c->GetId() }}
                NAME: {{ $c->GetName() }}
            </div>
        @endforeach

    </div>
</div>
<div class="row">
    <div class="container">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Lisa Category') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/admin/category/add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="category_name" class="col-md-4 col-form-label text-md-right">{{ __('Category Name') }}</label>

                            <div class="col-md-6">
                                <input id="category_name" type="text" class="form-control" name="category_name" value="{{ old('category_name') }}" required autocomplete="category_name" autofocus>

                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
