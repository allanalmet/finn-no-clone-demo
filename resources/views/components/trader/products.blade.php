@extends('layouts.master')

@section('content')
<div class="row">
    <div class="container">

        @foreach($products as $p)
            <div class="row">
                ID: {{ $p->GetId() }}
                NAME: {{ $p->GetName() }}
                DESCRIPTION: {{ $p->GetDescription() }}
                PRICE: {{ $p->GetPriceDouble() }}
                CategoryID: {{ $p->GetCategoryId() }}
            </div>
        @endforeach

    </div>
</div>
<div class="row">
    <div class="container">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Lisa Product') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/trader/product/add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="product_name" class="col-md-4 col-form-label text-md-right">{{ __('Product Name') }}</label>

                            <div class="col-md-6">
                                <input id="product_name" type="text" class="form-control" name="product_name" value="{{ old('product_name') }}" required autocomplete="product_name" autofocus>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_description" class="col-md-4 col-form-label text-md-right">{{ __('Product Description') }}</label>

                            <div class="col-md-6">
                                <input id="product_description" type="text" class="form-control" name="product_description" value="{{ old('product_description') }}" required autocomplete="product_description" autofocus>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_price" class="col-md-4 col-form-label text-md-right">{{ __('Product Price') }}</label>

                            <div class="col-md-6">
                                <input id="product_price" type="number" class="form-control" name="product_price" value="{{ old('product_price') }}" required autocomplete="product_price" autofocus>

                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
