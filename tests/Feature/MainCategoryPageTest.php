<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MainCategoryPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoadFirstCategoryPageSuccess()
    {
        $response = $this->get('/categories/1');
        $response->assertStatus(200);
        $response->assertSeeText("Login");
    }

    public function testLoadSecondCategoryPageSuccess()
    {
        $response = $this->get('/categories/2');
        $response->assertStatus(200);
    }

    public function testLoadThirdCategoryPageSuccess()
    {
        $response = $this->get('/categories/3');
        $response->assertStatus(200);
    }

    public function testLoadFourthCategoryPageSuccess()
    {
        $response = $this->get('/categories/4');
        $response->assertStatus(200);
    }

    public function testLoadFifthCategoryPageSuccess()
    {
        $response = $this->get('/categories/5');
        $response->assertStatus(200);
    }

    public function testLoadSixthCategoryPageSuccess()
    {
        $response = $this->get('/categories/6');
        $response->assertStatus(200);
    }
}