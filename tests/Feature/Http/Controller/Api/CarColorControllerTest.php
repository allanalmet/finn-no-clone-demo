<?php

namespace Tests\Feature\Http\Controller\Api;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CarColorControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_admin_car_color_index()
    {
        $response = $this->getJson('/api/admin/car/color/index');
        
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_colors')
            );
    }

    public function test_admin_car_color_store_returns_success()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/color/index', [
            'name' => 'red'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('status')
                    ->has('data')
            );
    } 
    
    public function test_admin_car_color_store_returns_error()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/color/index', [
            'test' => 'red'
        ]);

        $response->assertStatus(500);
    }
    
    public function test_admin_car_color_show_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->getJson('/api/admin/car/color/show/1');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_color')
            );
    }

    public function test_admin_car_color_edit_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->getJson('/api/admin/car/color/edit/1');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_color')
            );
    }

    public function test_admin_car_color_update_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/color/update/1', [
            'name' => 'red1'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data')
                    ->has('status')
            );
    }  
    
    public function test_admin_car_color_update_returns_error()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/color/update/1', [
            'test' => 'red1'
        ]);

        $response
            ->assertStatus(500)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('status')
                    ->has('error')
            );
    }      
}
