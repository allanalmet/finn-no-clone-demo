<?php

namespace Tests\Feature\Http\Controller\Api;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CarDriveControllerTest extends TestCase
{

    use WithoutMiddleware;
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_admin_car_drive_index()
    {
        $response = $this->getJson('/api/admin/car/drive/index');
        
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_drives')
            );
    }

    public function test_admin_car_drive_store_returns_success()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/drive/index', [
            'name' => 'automatic'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('status')
                    ->has('data')
            );
    }    
    
    public function test_admin_car_drive_store_returns_error()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/drive/index', [
            'test' => 'red'
        ]);

        $response->assertStatus(500);
    }    

    public function test_admin_car_drive_show_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->getJson('/api/admin/car/drive/show/1');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_drive')
            );
    }
    
    public function test_admin_car_drive_edit_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->getJson('/api/admin/car/drive/edit/1');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('car_drive')
            );
    }  

    public function test_admin_car_drive_update_returns_item()
    {
        $this->actingAs(new User());
        $response = $this->postJson('/api/admin/car/drive/update/1', [
            "name" => "test2"
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->has('data')
                    ->has('status')
            );
    }        
}
