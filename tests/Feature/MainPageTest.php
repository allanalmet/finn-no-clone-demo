<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MainPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoadMainPageSuccess()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}