<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\Mock\Service\HistoryService;
use App\Repositories\HistoryRepository;
use App\Models\History;

class HistoryServiceTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @return void
     */
    /*
    public function testGetAllNotNullTest()
    {
     
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));

        $history = $this->_historyService->GetAll();

        $this->assertTrue($history != null  ? true : false);
    }

    public function testGetAllIsNullTest()
    {
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));
        $history = $this->_historyService->GetAll();

        $this->assertFalse($history == null ? true : false);
    }

    public function testGetSingleIsObjectTest()
    {
     
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));

        $history = $this->_historyService->GetSingleById(1);

        $this->assertTrue(is_object($history));
    }

    public function testGetSingleIsNotNullTest()
    {
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));
        $history = $this->_historyService->GetSingleById(1);

        $this->assertTrue($history != null ? true : false);
    }

    public function testStoreSuccessTest()
    {
     
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));

        $history = $this->_historyService->Store([
            "name" => "1234",
            "id" => "1"
        ]);

        $this->assertTrue(is_object($history));
    }

    public function testStoreFailedTest()
    {
        $this->_historyService = new HistoryService(new HistoryRepository(new History()));
        $history = $this->_historyService->Store([
            "name" => "1234",
        ]);

        if ($history instanceof \InvalidArgumentException) 
        {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
    */
}
