const assert = require('chai').assert;
var expect = require('chai').expect

describe('Main Page', () => {

    it('Has login & register buttons', async () => {
        await browser.url(`http://localhost:8000`);

        const loginText = await $('=Login');
        const registerText = await $('=Register');
        expect(await loginText.getText()).to.equal('Login');
        expect(await registerText.getText()).to.equal('Register');
    });

    it('Has 5 categories', async () => {
        await browser.url(`http://localhost:8000`);

        const marketText = await $('=Turg');
        const trasnportText = await $('=Transport');
        const jobsText = await $('=Tööpakkumised');
        const realEstateText = await $('=Kinnisvara');
        const auctionText = await $('=Oksjon');
        expect(await marketText.getText()).to.equal('Turg');
        expect(await trasnportText.getText()).to.equal('Transport');
        expect(await jobsText.getText()).to.equal('Tööpakkumised');
        expect(await realEstateText.getText()).to.equal('Kinnisvara');
        expect(await auctionText.getText()).to.equal('Oksjon');
    });

    it('Has popular items title', async () => {
        await browser.url(`http://localhost:8000`);

        const popularItemsText = await $('=Populaarsemad');
        expect(await popularItemsText.getText()).to.equal('Populaarsemad');
    });
});

