<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TODO ALATI KUI ROUTE'I MUUDADA, TULEB TEHA "PHP ARTISAN OPTIMIZE"

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::namespace("Main")->group(function () {
    Route::get('/readfile', 'ReadFileController@index')->name('front.readfile');
    Route::get('/products', 'ProductController@index')->name('front.products');
    Route::get('/product/detail/{id}', 'ProductController@show')->name('front.product.detail');
    Route::get('/categories', 'CategoriesController@index')->name('front.categories');

    /*
    Route::get('/categories/{category}/{id}', 'CategoriesController@SubCategory')->name('category.sub.show');
    Route::post('/categories/{category}/{id}', 'categoriescontroller@GetSubCategoryData');
    */
    
    Route::get('/categories/{category}', 'CategoriesController@SubCategory')->name('categories.show');
    
});

Route::namespace("Api\Front")->prefix("api/front")->group(function() {
    Route::get('/search/{name}', 'SearchController@show');
    Route::get('/products', 'ProductsController@index');
    Route::get('/category/show/{category_id}', 'CategoriesController@show');
    Route::get('/categories', 'CategoriesController@index');
    Route::get('/categories/{category}', 'Categoriescontroller@show');
    Route::get('/categorytypes/{id}', 'CategoryTypesController@GetCategoryTypes');
    Route::get('/categorytypes/{category_id}/products', 'ProductFilterController@GetProductsByCategoryTypeId');
    Route::get('/category/{category_id}/products/all', 'ProductsController@GetProductsByCategoryId')->name('category.product.all');
    Route::get('/taxonomy/{product_id}', 'TaxonomyController@show');

    Route::get('/vehicle/info', 'VehicleController@show');
});

Route::namespace("Api\Dashboard")->prefix("api/admin")->group( function () {

});

Route::namespace("Api")->prefix("api")->group(function() {
    //Route::get('/routes/index', 'RoutesController@index');
});

    //TODO fotod-> admin-panel.png

/* Hoiame view meetodeid */
/* /admin/settings/index */
Route::namespace("Admin")->prefix("admin")->group( function () {
    Route::get('/index', 'DashboardController@index');
    Route::get('/settings/index', 'SettingsController@index');
    Route::get('/settings/create', 'SettingsController@create');
    Route::get('/settings/show/{id}', 'SettingsController@show');
    Route::get('/settings/edit/{id}', 'SettingsController@edit');

    Route::get('/categories/index', 'CategoriesController@index');
    Route::get('/categories/create', 'CategoriesController@create');
    Route::get('/categories/show/{id}', 'CategoriesController@show');
    Route::get('/categories/edit/{id}', 'CategoriesController@edit');

    Route::get('/products/index', 'ProductsController@index');
    Route::get('/products/create', 'ProductsController@create');
    Route::get('/products/show/{id}', 'ProductsController@show');
    Route::get('/products/edit/{id}', 'ProductsController@edit');

    Route::get('/taxonomies/index', 'TaxonomyController@index');
    Route::get('/taxonomies/create', 'TaxonomyController@create');
    Route::get('/taxonomies/show/{id}', 'TaxonomyController@show');
    Route::get('/taxonomies/edit/{id}', 'TaxonomyController@edit');

    Route::get('/car/color/index', 'CarColorController@index');
    Route::get('/car/color/create', 'CarColorController@create');
    Route::get('/car/color/show/{id}', 'CarColorController@show');
    Route::get('/car/color/edit/{id}', 'CarColorController@edit');

    Route::get('/car/drive/index', 'CarDriveController@index');
    Route::get('/car/drive/create', 'CarDriveController@create');
    Route::get('/car/drive/show/{id}', 'CarDriveController@show');
    Route::get('/car/drive/edit/{id}', 'CarDriveController@edit');

    Route::get('/car/transmission/index', 'CarTransmissionController@index');
    Route::get('/car/transmission/create', 'CarTransmissionController@create');
    Route::get('/car/transmission/show/{id}', 'CarTransmissionController@show');
    Route::get('/car/transmission/edit/{id}', 'CarTransmissionController@edit');

    Route::get('/sale/type/index', 'SaleTypeController@index');
    Route::get('/sale/type/create', 'SaleTypeController@create');
    Route::get('/sale/type/show/{id}', 'SaleTypeController@show');
    Route::get('/sale/type/edit/{id}', 'SaleTypeController@edit');
   

});


/* URL: /api/admin/settings/index */
Route::namespace("Api\Dashboard")->middleware(['cors'])->prefix("api/admin")->group(function() {
    Route::get('/settings/index', 'SettingsController@index');
    Route::get('/settings/create', 'SettingsController@create');
    Route::get('/settings/show/{id}', 'SettingsController@show');
    Route::get('/settings/edit/{id}', 'SettingsController@edit');
    Route::post('/settings/index', 'SettingsController@store');
    Route::post('/settings/update', 'SettingsController@update');
    Route::get('/settings/destroy', 'SettingsController@destroy');

    Route::get('/categories/index', 'CategoriesController@index');
    Route::get('/categories/create', 'CategoriesController@create');
    Route::post('/categories/index', 'CategoriesController@store');
    Route::get('/categories/show/{id}', 'CategoriesController@show');
    Route::get('/categories/edit/{id}', 'CategoriesController@edit');
    Route::post('/categories/update/{id}', 'CategoriesController@update');
    Route::get('/categories/destroy/{id}', 'CategoriesController@destroy');
    
    Route::get('/products/index', 'ProductsController@index');
    Route::get('/products/create', 'ProductsController@create');
    Route::post('/products/index', 'ProductsController@store');
    Route::get('/products/show/{id}', 'ProductsController@show');
    Route::get('/products/edit/{id}', 'ProductsController@edit');
    Route::post('/products/update/{id}', 'ProductsController@update');
    Route::get('/products/destroy/{id}', 'ProductsController@destroy');

    Route::get('/taxonomies/index', 'TaxonomiesController@index');
    Route::get('/taxonomies/create', 'TaxonomiesController@create');
    Route::post('/taxonomies/index', 'TaxonomiesController@store');
    Route::get('/taxonomies/show/{id}', 'TaxonomiesController@show');
    Route::get('/taxonomies/edit/{id}', 'TaxonomiesController@edit');
    Route::post('/taxonomies/update/{id}', 'TaxonomiesController@update');
    Route::get('/taxonomies/destroy/{id}', 'TaxonomiesController@destroy');
    Route::get('/taxonomies/parent/{id}', 'TaxonomiesController@getByParentId');

    Route::get('/car/color/index', 'CarColorController@index');
    Route::get('/car/color/create', 'CarColorController@create');
    Route::post('/car/color/index', 'CarColorController@store');
    Route::get('/car/color/show/{id}', 'CarColorController@show');
    Route::get('/car/color/edit/{id}', 'CarColorController@edit');
    Route::post('/car/color/update/{id}', 'CarColorController@update');
    Route::get('/car/color/destroy/{id}', 'CarColorController@destroy');

    Route::get('/car/drive/index', 'CarDriveController@index');
    Route::get('/car/drive/create', 'CarDriveController@create');
    Route::post('/car/drive/index', 'CarDriveController@store');
    Route::get('/car/drive/show/{id}', 'CarDriveController@show');
    Route::get('/car/drive/edit/{id}', 'CarDriveController@edit');
    Route::post('/car/drive/update/{id}', 'CarDriveController@update');
    Route::get('/car/drive/destroy/{id}', 'CarDriveController@destroy');

    Route::get('/car/transmission/index', 'CarTransmissionController@index');
    Route::get('/car/transmission/create', 'CarTransmissionController@create');
    Route::post('/car/transmission/index', 'CarTransmissionController@store');
    Route::get('/car/transmission/show/{id}', 'CarTransmissionController@show');
    Route::get('/car/transmission/edit/{id}', 'CarTransmissionController@edit');
    Route::post('/car/transmission/update/{id}', 'CarTransmissionController@update');
    Route::get('/car/transmission/destroy/{id}', 'CarTransmissionController@destroy');

    Route::get('/sale/type/index', 'SaleTypeController@index');
    Route::get('/sale/type/create', 'SaleTypeController@create');
    Route::post('/sale/type/index', 'SaleTypeController@store');
    Route::get('/sale/type/show/{id}', 'SaleTypeController@show');
    Route::get('/sale/type/edit/{id}', 'SaleTypeController@edit');
    Route::post('/sale/type/update/{id}', 'SaleTypeController@update');
    Route::get('/sale/type/destroy/{id}', 'SaleTypeController@destroy');


});

Route::namespace("User")->prefix("user")->group( function () {
    Route::get('/index', 'DashboardController@index'); 
    Route::get('/settings/index', 'SettingsController@index');
    Route::get('/settings/create', 'SettingsController@create');
    Route::get('/settings/show/{id}', 'SettingsController@show');
    Route::get('/settings/edit/{id}', 'SettingsController@edit');

    Route::get('/history/index', 'HistoryController@index');
    Route::get('/history/create', 'HistoryController@create');
    Route::get('/history/show/{id}', 'HistoryController@show');
    Route::get('/history/edit/{id}', 'HistoryController@edit');
});

Route::namespace("Api\Dashboard")->prefix("api/user")->group( function () {
    Route::get('/history/index', 'HistoryController@index');
    Route::get('/history/create', 'HistoryController@create');
    Route::post('/history/index', 'HistoryController@store');
    Route::get('/history/show/{id}', 'HistoryController@show');
    Route::get('/history/edit/{id}', 'HistoryController@edit');
    Route::post('/history/update/{id}', 'HistoryController@update');
    Route::get('/history/destroy/{id}', 'HistoryController@destroy');


    Route::get('/settings/index', 'SettingsController@index');
    Route::get('/settings/create', 'SettingsController@create');
    Route::post('/settings/index', 'SettingsController@store');
    Route::get('/settings/show/{id}', 'SettingsController@show');
    Route::get('/settings/edit/{id}', 'SettingsController@edit');
    Route::post('/settings/update/{id}', 'SettingsController@update');
    Route::get('/settings/destroy/{id}', 'SettingsController@destroy');


});

Route::namespace("Trader")->prefix("trader")->group( function () {
    Route::get('/index', 'DashboardController@index'); 
    Route::get('/categories/index', 'CategoriesController@index');
    Route::get('/categories/create', 'CategoriesController@create');
    Route::get('/categories/show/{id}', 'CategoriesController@show');
    Route::get('/categories/edit/{id}', 'CategoriesController@edit');

    Route::get('/products/index', 'ProductsController@index');
    Route::get('/products/create', 'ProductsController@create');
    Route::get('/products/show/{id}', 'ProductsController@show');
    Route::get('/products/edit/{id}', 'ProductsController@edit');

});

Route::namespace("Api\Dashboard")->prefix("api/trader")->group( function () {
    Route::get('/categories/index', 'CategoriesController@index');
    Route::get('/categories/create', 'CategoriesController@create');
    Route::post('/categories/index', 'CategoriesController@store');
    Route::get('/categories/show/{id}', 'CategoriesController@show');
    Route::get('/categories/edit/{id}', 'CategoriesController@edit');
    Route::post('/categories/update/{id}', 'CategoriesController@update');
    Route::get('/categories/destroy/{id}', 'CategoriesController@destroy');

    Route::get('/products/index', 'ProductsController@index');
    Route::get('/products/create', 'ProductsController@create');
    Route::post('/products/index', 'ProductsController@store');
    Route::get('/products/show/{id}', 'ProductsController@show');
    Route::get('/products/edit/{id}', 'ProductsController@edit');
    Route::post('/products/update/{id}', 'ProductsController@update');
    Route::get('/products/destroy/{id}', 'ProductsController@destroy');


});
